module.exports = {
  root: true,
  env: {
    node: true,
  },
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "project": "tsconfig.json",
    "sourceType": "module",
    ecmaVersion: 2020,
  },
  plugins: ['import', 'prettier', '@typescript-eslint'],
  extends: [
    "plugin:@typescript-eslint/eslint-recommended",
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        trailingComma: 'all',
      },
    ],
    quotes: ['error', 'single'],
    'import/order': [
      'error',
      {
        groups: [
          ['builtin', 'internal'], // Built-in and external types are first
          ['external', 'sibling', 'parent'], // Then sibling and parent types. They can be mingled together
          'index', // Then the index file
        ],
        'newlines-between': 'always',
        alphabetize: {
          order: 'asc',
          caseInsensitive: true,
        },
      },
    ],
    'comma-dangle': ['error', 'always-multiline'],
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
};
