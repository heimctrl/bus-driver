FROM node:18-alpine as build

RUN apk add --no-cache g++ linux-headers make py3-pip

WORKDIR /opt/bus-driver
COPY package.json yarn.lock .yarnrc ./

ENV YARN_CACHE_FOLDER /tmp/yarn
RUN NODE_ENV=development yarn --frozen-lockfile --non-interactive

COPY ./* ./
COPY ./src ./src
COPY ./config ./config
RUN yarn build \
    && rm -rf node_modules && yarn --production --frozen-lockfile --non-interactive

FROM node:18-alpine as final
COPY --from=build /opt/bus-driver /opt/bus-driver

ENV NODE_ENV=production \
    PORT=8123 \
    DATABASE_LOCATION=/opt/bus-driver/database \
    SERVER_ROOM= \
    SERVER_HOMEASSISTANT_URL= \
    SERVER_HOMEASSISTANT_MQTT_BROKER= \
    SERVER_HOMEASSISTANT_MQTT_USERNAME= \
    SERVER_HOMEASSISTANT_MQTT_PASSWORD= \
    SERVER_HOMEASSISTANT_DEFAULTDASHBOARD=

WORKDIR /opt/bus-driver

ENTRYPOINT ["node", "./build/index.js"]
