#!/bin/bash

sudo apt-get install -y i2c-tools

# install python packages
pip install RPi.GPIO spidev

# install smbus library
sudo apt-get install -y python-smbus python-serial

# install python can libraries
pip install python-can
 