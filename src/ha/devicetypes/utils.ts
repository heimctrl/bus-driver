import { Device } from '../../devices/models/Device.js';
import { MqttDeviceType, MqttDevice } from '../ha.mqtt.types.js';

export function getVersions(
  device: Pick<Device, 'hardwareVersion' | 'firmwareVersion'>,
): Pick<MqttDevice, 'hw_version' | 'sw_version'> {
  const { hardwareVersion, firmwareVersion } = device;
  return {
    hw_version: typeof hardwareVersion === 'number' ? `v${hardwareVersion}` : undefined,
    sw_version: typeof firmwareVersion === 'number' ? `v${firmwareVersion >> 8}.${firmwareVersion & 0xff}` : undefined,
  };
}

export function getConfigTopic(deviceType: MqttDeviceType, name: string) {
  return `homeassistant/${deviceType}/${name}/config`;
}

export function getConfigTopicWithDevice(deviceType: MqttDeviceType, deviceName: string, name: string) {
  return `homeassistant/${deviceType}/${deviceName}/${name}/config`;
}
