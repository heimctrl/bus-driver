import { getCommonDeviceProps } from './getCommonDeviceProps.js';
import { getConfigTopicWithDevice, getVersions } from './utils.js';
import { PWMDimmerValue } from '../../devices/devicetypes/PWMDimmer/PWMDimmerValue.js';
import { Device } from '../../devices/models/Device.js';
import { toHex } from '../../log/utils/toHex.js';
import {
  MqttConfigLight,
  MqttDeviceType,
  MqttPayloadLight,
  DeviceConfig,
  HaContext,
  DeviceConfigFactory,
  MqttConfigNumber,
} from '../ha.mqtt.types.js';
import { HaData } from '../ha.types.js';

export const PWMDimmerCtrlConfigFactory: DeviceConfigFactory = (
  room: string,
  device: Device,
): DeviceConfig<MqttConfigLight | MqttConfigNumber>[] => {
  return [...getChannelConfigs(room, device, 'a'), ...getChannelConfigs(room, device, 'b')];
};

function getChannelConfigs(
  room: string,
  device: Device,
  channel: string,
): DeviceConfig<MqttConfigLight | MqttConfigNumber>[] {
  const { common, internal } = getCommonDeviceProps(MqttDeviceType.light, room, device, channel);
  const { deviceName } = internal;

  const haDevice = {
    identifiers: deviceName,
    name: `${room} ${toHex(device.serial)}`,
    manufacturer: 'HeimCtrl',
    model: 'PWMDimmerCtrl',
    ...getVersions(device),
    suggested_area: room,
  };

  const functionName = `channel_${channel}`;
  const stateTopic = `${internal.topicBase}/${functionName}/state`;
  const valueTopic = `${internal.topicBase}/${functionName}/value`;

  return [
    {
      topics: {
        configTopic: getConfigTopicWithDevice(MqttDeviceType.light, deviceName, functionName),
        stateTopic,
        valueTopic,
      },
      config: {
        ...common,
        schema: 'json',
        name: `Channel ${channel.toUpperCase()}`,
        state_topic: stateTopic,
        command_topic: valueTopic,
        payload_on: 'on',
        payload_off: 'off',
        brightness: true,
        color_mode: true,
        supported_color_modes: ['brightness'],
        device: haDevice,
      },
      handlers: {
        processHaMessage: (haContext: HaContext, topic: string, message: Buffer) => {
          if (topic !== valueTopic) {
            return;
          }
          const payload: MqttPayloadLight = JSON.parse(message.toString());

          let value = payload.state === 'ON' ? 100 : 0;
          if (typeof payload.brightness === 'number') {
            value = (payload.brightness * 100) / 255;
          }

          const deviceValue: PWMDimmerValue = {
            version: 1,
            channelALevel: value,
            channelBLevel: 0,
          };
          const event: HaData = {
            deviceSerial: device.serial,
            value: deviceValue,
          };
          haContext.dataEmitter.emit(event);
        },
        processDeviceMessage: (haContext: HaContext, data: HaData): void => {
          const { value } = data;
          const { channelALevel } = value as PWMDimmerValue;

          const state = channelALevel > 0 ? 'ON' : 'OFF';
          const brightness = channelALevel > 0 ? Math.min(255, (channelALevel * 255) / 100) : undefined;
          const payload: MqttPayloadLight = {
            state,
            brightness,
          };
          haContext.publishMessage(stateTopic, JSON.stringify(payload));
        },
      },
    },
    {
      topics: {
        configTopic: getConfigTopicWithDevice(MqttDeviceType.light, deviceName, functionName),
        stateTopic,
        valueTopic,
      },
      config: {
        ...common,
        schema: 'json',
        name: `Channel ${channel.toUpperCase()} Fade Rate`,
        state_topic: stateTopic,
        command_topic: valueTopic,
        payload_on: 'on',
        payload_off: 'off',
        brightness: true,
        color_mode: true,
        supported_color_modes: ['brightness'],
        device: haDevice,
      },
      handlers: {
        processHaMessage: (haContext: HaContext, topic: string, message: Buffer) => {
          if (topic !== valueTopic) {
            return;
          }
          const payload: MqttPayloadLight = JSON.parse(message.toString());

          let value = payload.state === 'ON' ? 100 : 0;
          if (typeof payload.brightness === 'number') {
            value = (payload.brightness * 100) / 255;
          }

          const deviceValue: PWMDimmerValue = {
            version: 1,
            channelALevel: value,
            channelBLevel: 0,
          };
          const event: HaData = {
            deviceSerial: device.serial,
            value: deviceValue,
          };
          haContext.configEmitter.emit(event);
        },
        processDeviceMessage: (haContext: HaContext, data: HaData): void => {
          const { value } = data;
          const { channelALevel } = value as PWMDimmerValue;

          const state = channelALevel > 0 ? 'ON' : 'OFF';
          const brightness = channelALevel > 0 ? Math.min(255, (channelALevel * 255) / 100) : undefined;
          const payload: MqttPayloadLight = {
            state,
            brightness,
          };
          haContext.publishMessage(stateTopic, JSON.stringify(payload));
        },
      },
    },
  ];
}
