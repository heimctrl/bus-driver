import { getCommonDeviceProps } from './getCommonDeviceProps.js';
import { getConfigTopicWithDevice, getVersions } from './utils.js';
import { SwitchValue } from '../../devices/devicetypes/Switch/SwitchValue.js';
import { Device } from '../../devices/models/Device.js';
import { toHex } from '../../log/utils/toHex.js';
import {
  MqttConfigDeviceAutomation,
  MqttDeviceAutomationType,
  MqttDeviceType,
  MqttDeviceAutomationSubtype,
  DeviceConfig,
  HaContext,
  DeviceConfigFactory,
} from '../ha.mqtt.types.js';
import { HaData } from '../ha.types.js';

export const SwitchCtrlConfigFactory: DeviceConfigFactory = (
  room: string,
  device: Device,
): DeviceConfig<MqttConfigDeviceAutomation>[] => {
  const { common, internal } = getCommonDeviceProps(MqttDeviceType.device_automation, room, device, 'short_press');
  const stateTopic = `${internal.topicBase}/state`;
  const { deviceName } = internal;
  const functionName = `${deviceName}_single`;

  return [
    {
      topics: {
        configTopic: getConfigTopicWithDevice(MqttDeviceType.device_automation, deviceName, functionName),
        stateTopic,
      },
      config: {
        ...common,
        name: 'Short Press',
        automation_type: 'trigger',
        topic: stateTopic,
        payload: 'press',
        type: MqttDeviceAutomationType.button_short_press,
        subtype: MqttDeviceAutomationSubtype.button_1,
        device: {
          identifiers: deviceName,
          name: `${room} ${toHex(device.serial)}`,
          manufacturer: 'HeimCtrl',
          model: 'SwitchCtrl',
          ...getVersions(device),
          suggested_area: room,
        },
      },
      handlers: {
        processHaMessage: (haContext: HaContext, topic: string, message: Buffer) => {},
        processDeviceMessage: (haContext: HaContext, data: HaData): void => {
          const { value } = data;
          const { event } = value as SwitchValue;

          if (event === 0) {
            haContext.publishMessage(stateTopic, 'press');
          }
        },
      },
    },
  ];
};
