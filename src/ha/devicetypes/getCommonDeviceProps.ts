import { Device } from '../../devices/models/Device.js';
import { toHex } from '../../log/utils/toHex.js';
import { MqttConfigBase, MqttDeviceType, MqttConfigInternals } from '../ha.mqtt.types.js';

export function getCommonDeviceProps(
  haDeviceType: MqttDeviceType,
  room: string,
  device: Device,
  action?: string,
): { common: MqttConfigBase; internal: MqttConfigInternals } {
  const deviceName = `${room}_${toHex(device.serial)}`;
  const topicBase = `heimctrl/${deviceName}`;
  const uniqueId = action ? `${toHex(device.serial)}_${action}` : toHex(device.serial);

  return {
    internal: {
      deviceType: haDeviceType,
      topicBase,
      deviceName,
    },
    common: {
      unique_id: uniqueId,
      object_id: action || uniqueId,
      availability: {
        topic: `${topicBase}/available`,
        payload_available: 'online',
        payload_not_available: 'offline',
      },
    },
  };
}
