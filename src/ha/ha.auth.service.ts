import { LogService } from '../log/log.service.js';
import { Injectable } from '@nestjs/common';

@Injectable()
export class HAAuthService {
  private log: LogService;

  constructor(log: LogService) {
    this.log = log.getPrefixedLogger('[HA_AUTH]');
  }
}
