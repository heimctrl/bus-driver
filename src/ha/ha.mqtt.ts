import { PWMDimmerCtrlConfigFactory } from './devicetypes/PWMDimmerCtrl.factory.js';
import { SwitchCtrlConfigFactory } from './devicetypes/SwitchCtrl.factory.js';
import { DeviceConfigFactory } from './ha.mqtt.types.js';
import { DeviceType } from '../devices/constants/DeviceType.js';
import { Device } from '../devices/models/Device.js';

export const DeviceConfigFactories: Record<DeviceType, DeviceConfigFactory> = {
  [DeviceType.SwitchCtrl]: SwitchCtrlConfigFactory,
  [DeviceType.PWMDimmerCtrl]: PWMDimmerCtrlConfigFactory,
};

export function getDeviceConfig(room: string, device: Device | null): ReturnType<DeviceConfigFactory> | null {
  if (!device) {
    return null;
  }
  const configFactory = DeviceConfigFactories[device.deviceType];
  return configFactory ? configFactory(room, device) : null;
}
