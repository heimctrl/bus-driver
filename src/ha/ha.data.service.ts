import { getDeviceConfig } from './ha.mqtt.js';
import { DeviceConfig, MqttConfigBase } from './ha.mqtt.types.js';
import { ConfigData, HaData, HADataServiceType } from './ha.types.js';
import { DevicesRepository } from '../devices/devices.repository.js';
import { Device } from '../devices/models/Device.js';
import { CONFIGS } from '../enums/config-enums.js';
import { LogService } from '../log/log.service.js';
import { toHex } from '../log/utils/toHex.js';
import { TypedEvent } from '../utils/TypedEvent.js';
import { Inject, Injectable } from '@nestjs/common';
import mqtt, { Packet } from 'mqtt';

export interface DataServiceOptions {
  room: string;
  homeassistant: {
    mqtt: MQTTOptions;
  };
}

export interface MQTTOptions {
  broker: string;
  username: string;
  password: string;
}

/**
 * Interact with the home assistant backend.
 * Send and receive events.
 */
@Injectable()
export class HADataService implements HADataServiceType {
  private client: mqtt.MqttClient;
  private topicMap: Record<string, DeviceConfig<MqttConfigBase>> = {};

  public configEmitter: TypedEvent<ConfigData> = new TypedEvent<ConfigData>();
  public dataEmitter: TypedEvent<HaData> = new TypedEvent<HaData>();

  constructor(
    private log: LogService,
    @Inject(CONFIGS.server) private options: DataServiceOptions,
    private db: DevicesRepository,
  ) {
    this.log = log.getPrefixedLogger('[HA_DATA]');

    this.log.info('MQTT connecting');
    this.client = mqtt.connect(this.options.homeassistant.mqtt.broker, {
      username: this.options.homeassistant.mqtt.username,
      password: this.options.homeassistant.mqtt.password,
    });

    this.client.on('error', (err: Error) => {
      this.log.error('MQTT error:', err);
    });

    this.client.on('connect', () => {
      this.log.info('MQTT connected');

      // send device config for  all registered devices on startup
      for (const device of this.db.getDevices()) {
        this.handleDevicesChanged('add', device);
      }
    });

    this.client.on('message', this.handleHAMessage);

    this.db.on('remove', (device) => this.handleDevicesChanged('remove', device));
  }

  private publishMessage = (topic: string, message: string | Buffer): Promise<Packet | null> => {
    return new Promise((resolve) => {
      this.log.debug('PUB', topic, JSON.stringify(message));
      this.client.publish(topic, message, (error?: Error, packet?: Packet) => {
        if (error) {
          this.log.error('Pub Error:', error.message);
          resolve(null);
        } else {
          this.log.debug('Pub Success');
          resolve(packet || null);
        }
      });
    });
  };

  private handleDevicesChanged(event: 'add' | 'remove' | 'online' | 'offline', device: Device | null) {
    const deviceConfigs = getDeviceConfig(this.options.room, device);
    if (!device || !deviceConfigs) {
      return;
    }

    for (const deviceConfig of deviceConfigs) {
      const { config: mqttConfig, topics } = deviceConfig;

      const configTopic = topics.configTopic;
      this.log.debug(`Device ${event}`, configTopic);

      switch (event) {
        case 'add':
          this.publishMessage(configTopic, JSON.stringify(mqttConfig));
          break;
        case 'online': {
          this.publishMessage(mqttConfig.availability.topic, mqttConfig.availability.payload_available!);
          // subscribe to receive value updates
          const { valueTopic } = topics;
          if (valueTopic) {
            this.client.subscribe(valueTopic, (err?: Error) => {
              if (err) {
                this.log.error(`Subscribe device ${toHex(device.serial)} error:`, err);
                return;
              }
              this.log.info(`Device ${toHex(device.serial)} subscribed for value updates`);
              this.topicMap[valueTopic] = deviceConfig;
            });
          }
          break;
        }
        case 'offline':
          this.publishMessage(mqttConfig.availability.topic, mqttConfig.availability.payload_not_available!);
          // unsubscribe from value updates
          if (topics.valueTopic) {
            this.client.unsubscribe(topics.valueTopic, (err?: Error) => {
              if (err) {
                this.log.error(`Unsubscribe device ${toHex(device.serial)} error:`, err);
                return;
              }
              this.log.info(`Device ${toHex(device.serial)} unsubscribed from value updates`);
            });
          }
          break;
        case 'remove':
          this.log.debug('PUB', configTopic, JSON.stringify(mqttConfig));
          break;
        default:
      }
    }
  }

  private receiveConfig(cnf: ConfigData) {
    this.log.debug('Received Configuration:', cnf);
    this.configEmitter.emit(cnf);
  }

  /**
   * Handle a message from Home Assistant
   * @param topic - topic on which the message was receiived
   * @param payload - message payload
   * @returns
   */
  private handleHAMessage = (topic: string, payload: Buffer) => {
    const message = payload.toString('utf8');
    this.log.debug(`Received message on topic "${topic}":`, message);

    const deviceConfig = this.topicMap[topic];
    if (!deviceConfig) {
      // ignore messages on unknown topics
      return;
    }

    const {
      handlers: { processHaMessage },
    } = deviceConfig;
    if (!processHaMessage) {
      this.log.warn(`No processHaMessage handler for device "${deviceConfig.config.name}"`);
      return;
    }

    processHaMessage(
      {
        publishMessage: this.publishMessage,
        dataEmitter: this.dataEmitter,
        configEmitter: this.configEmitter,
      },
      topic,
      payload,
    );
  };

  /**
   * Sent on startup when a known device sends a StartInfo message
   * @param device
   */
  public deviceStarted(serial: number) {
    this.log.info(`Device 0x${toHex(serial)} started, notify HA`);
    // mark device as available in HA
    this.handleDevicesChanged('online', this.db.findBySerial(serial));
  }

  /**
   * A device is sending a state value
   * @param evt
   */
  public sendState(evt: HaData) {
    const device: Device | null = this.db.findBySerial(evt.deviceSerial);
    const deviceConfigs = getDeviceConfig(this.options.room, device);

    if (!deviceConfigs) {
      this.log.warn('No configs for device:', evt);
      return;
    }

    for (const deviceConfig of deviceConfigs) {
      const processDeviceMessage = deviceConfig?.handlers.processDeviceMessage;
      const stateTopic = deviceConfig?.topics.stateTopic;
      this.log.debug(`Try to send state to ${stateTopic}:`, evt);

      if (stateTopic && processDeviceMessage) {
        this.log.info(`Device ${toHex(evt.deviceSerial)} state update:`, evt);
        processDeviceMessage(
          {
            publishMessage: this.publishMessage,
            dataEmitter: this.dataEmitter,
            configEmitter: this.configEmitter,
          },
          evt,
        );
      }
    }
  }
}
