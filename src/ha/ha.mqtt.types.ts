import { ConfigData, HaData } from './ha.types.js';
import { Device } from '../devices/models/Device.js';
import { TypedEvent } from '../utils/TypedEvent.js';
import { Packet } from 'mqtt';

export interface DeviceConfig<TConfig extends MqttConfigBase> {
  topics: {
    configTopic: string;
    /**
     * Send device state updates to HA
     */
    stateTopic: string;
    /**
     * Receive value updates from HA. Optional.
     */
    valueTopic?: string;
  };
  config: TConfig;
  /**
   * Transformers for messaging between HA and HeimCtrl
   */
  handlers: {
    /**
     * Transform a HeimCtrl message to a HA message
     * @param message
     * @returns unknown
     */
    processDeviceMessage?: (haContext: HaContext, data: HaData) => void;
    /**
     * Process a message received from HA
     * @param haService
     * @param topic
     * @param message
     * @returns void
     */
    processHaMessage: (haService: HaContext, topic: string, message: Buffer) => void;
  };
}
export type DeviceConfigFactory = (room: string, device: Device) => DeviceConfig<MqttConfigBase>[];

export interface HaContext {
  configEmitter: TypedEvent<ConfigData>;
  dataEmitter: TypedEvent<HaData>;
  publishMessage: (topic: string, message: string | Buffer) => Promise<Packet | null>;
}

/**
 * Used to shuttle some common values around
 */
export interface MqttConfigInternals {
  deviceType: MqttDeviceType;
  topicBase: string;
  deviceName: string;
}

/**
 * Common config values for all device types
 */
export interface MqttConfigBase {
  unique_id: string;
  object_id?: string;
  name?: string;
  device?: MqttDevice;
  availability: MqttConfigAvailability;
  availability_mode?: MqttConfigAvailabilityMode;
  qos?: number;
  optimistic?: boolean;
  retain?: boolean;
}

export enum MqttDeviceType {
  switch = 'switch',
  button = 'button',
  light = 'light',
  sensor = 'sensor',
  binary_sensor = 'binary_sensor',
  motion = 'motion',
  temperature = 'temperature',
  humidity = 'humidity',
  device_automation = 'device_automation',
  number = 'number',
}

export interface MqttDevice {
  /**
   * A link to the webpage that can manage the configuration of this device. Can be either an HTTP or HTTPS link.
   */
  configuration_url?: string;
  connections?: MqttConnection[];
  hw_version?: string;
  sw_version?: string;
  /**
   * A list of IDs that uniquely identify the device. For example a serial number.
   */
  identifiers?: string;
  manufacturer?: string;
  model?: string;
  name?: string;
  suggested_area?: string;
  via_device?: string;
}

/**
 * For example: ["mac", "02:5b:26:a8:dc:12"]
 */
export type MqttConnection = [connection_type: string, connection_identifier: string];

export interface MqttConfigAvailability {
  topic: string;
  payload_available: string;
  payload_not_available: string;
  /**
   * Format: {{ json_value.state }} if message to topic like {"state":"online"}
   */
  value_template?: string;
}

export enum MqttConfigAvailabilityMode {
  all = 'all',
  any = 'any',
  latest = 'latest',
}

export interface MqttConfigLight extends MqttConfigBase {
  schema: 'default' | 'json' | 'template';
  command_topic: string;
  command_value_template?: string;
  state_topic?: string;
  state_value_template?: string;

  payload_on: string;
  payload_off: string;

  brightness?: boolean;
  brightness_command_topic?: string;
  brightness_command_template?: string;
  brightness_state_topic?: string;
  brightness_value_template?: string;
  brightness_scale?: number;

  color_mode?: boolean;
  supported_color_modes?: ('onoff' | 'brightness' | 'color_temp' | 'hs' | 'xy' | 'rgb' | 'rgbw' | 'rgbww' | 'white')[];
  rgb_command_topic?: string;
  rgb_command_template?: string;
  rgb_state_topic?: string;
  rgb_value_template?: string;

  rgbw_command_topic?: string;
  rgbw_command_template?: string;
  rgbw_state_topic?: string;
  rgbw_value_template?: string;
}

export interface MqttPayloadLight {
  state: 'ON' | 'OFF';
  brightness?: number;
}

export interface MqttConfigSwitch extends MqttConfigBase {
  command_topic: string;
  state_topic?: string;
  state_value_template?: string;
  payload_on?: string;
  payload_off?: string;
  device_class: MqttDeviceClassSwitch;
}

export enum MqttDeviceClassSwitch {
  None = 'None',
  outlet = 'outlet',
  switch = 'switch',
}

export interface MqttConfigButton extends MqttConfigBase {
  command_topic: string;
  state_topic?: string;
  state_value_template?: string;
  payload_press?: string;
}

export interface MqttConfigBinarySensor extends MqttConfigBase {
  command_topic: string;
  state_topic?: string;
  state_value_template?: string;
  device_class: MqttDeviceClassBinarySensor;
  /**
   * If set, it defines the number of seconds after the sensor’s state expires, if it’s not updated.
   * After expiry, the sensor’s state becomes unavailable.
   * Default the sensors state never expires.
   */
  expire_after?: number;
  /**
   * For sensors that only send on state updates (like PIRs), this variable sets a delay in seconds
   * after which the sensor’s state will be updated back to off.
   */
  off_delay?: number;
  /**
   * The string that represents the on state. It will be compared to the message in the state_topic
   * (see value_template for details)
   */
  payload_on?: string;
  /**
   * The string that represents the off state. It will be compared to the message in the state_topic
   * (see value_template for details)
   */
  payload_off?: string;
}

export enum MqttDeviceClassBinarySensor {
  None = 'None',
  battery = 'battery',
  battery_charging = 'battery_charging',
  carbon_monoxide = 'carbon_monoxide',
  cold = 'cold',
  connectivity = 'connectivity',
  door = 'door',
  garage_door = 'garage_door',
  gas = 'gas',
  heat = 'heat',
  light = 'light',
  lock = 'lock',
  moisture = 'moisture',
  motion = 'motion',
  moving = 'moving',
  occupancy = 'occupancy',
  opening = 'opening',
  plug = 'plug',
  power = 'power',
  presence = 'presence',
  problem = 'problem',
  running = 'running',
  safety = 'safety',
  smoke = 'smoke',
  sound = 'sound',
  tamper = 'tamper',
  update = 'update',
  vibration = 'vibration',
  window = 'window',
}

export interface MqttConfigDeviceAutomation extends MqttConfigBase {
  automation_type: 'trigger';
  topic: string;
  payload?: string;
  type: MqttDeviceAutomationType;
  subtype: MqttDeviceAutomationSubtype;
  device: MqttDevice;
}

export enum MqttDeviceAutomationType {
  button_short_press = 'button_short_press',
  button_short_release = 'button_short_release',
  button_long_press = 'button_long_press',
  button_long_release = 'button_long_release',
  button_double_press = 'button_double_press',
  button_triple_press = 'button_triple_press',
  button_quadruple_press = 'button_quadruple_press',
  button_quintuple_press = 'button_quintuple_press',
}

export enum MqttDeviceAutomationSubtype {
  turn_on = 'turn_on',
  turn_off = 'turn_off',
  button_1 = 'button_1',
  button_2 = 'button_2',
  button_3 = 'button_3',
  button_4 = 'button_4',
  button_5 = 'button_5',
  button_6 = 'button_6',
}

export interface MqttConfigNumber extends MqttConfigBase {
  entity_category: 'config';
  command_topic: string;
  device_class?: MqttDeviceClassNumber;
  min?: number;
  max?: number;
  mode?: 'box' | 'slider';
  /**
   * float (optional, default: 1)
   * Step value. Smallest value 0.001.
   */
  step?: number;
  unit_of_measurement?: string;
}

export enum MqttDeviceClassNumber {
  None = 'None',
  apparent_power = 'apparent_power',
  aqi = 'aqi',
  atmospheric_pressure = 'atmospheric_pressure',
  battery = 'battery',
  carbon_dioxide = 'carbon_dioxide',
  carbon_monoxide = 'carbon_monoxide',
  current = 'current',
  data_rate = 'data_rate',
  data_size = 'data_size',
  distance = 'distance',
  energy = 'energy',
  energy_storage = 'energy_storage',
  frequency = 'frequency',
  gas = 'gas',
  humidity = 'humidity',
  illuminance = 'illuminance',
  irradiance = 'irradiance',
  moisture = 'moisture',
  monetary = 'monetary',
  nitrogen_dioxide = 'nitrogen_dioxide',
  nitrogen_monoxide = 'nitrogen_monoxide',
  nitrous_oxide = 'nitrous_oxide',
  ozone = 'ozone',
  pm1 = 'pm1',
  pm10 = 'pm10',
  pm25 = 'pm25',
  power_factor = 'power_factor',
  power = 'power',
  precipitation = 'precipitation',
  precipitation_intensity = 'precipitation_intensity',
  pressure = 'pressure',
  reactive_power = 'reactive_power',
  signal_strength = 'signal_strength',
  sound_pressure = 'sound_pressure',
  speed = 'speed',
  sulphur_dioxide = 'sulphur_dioxide',
  temperature = 'temperature',
  volatile_organic_compounds = 'volatile_organic_compounds',
  voltage = 'voltage',
  volume = 'volume',
  volume_storage = 'volume_storage',
  water = 'water',
  weight = 'weight',
  wind_speed = 'wind_speed',
}
