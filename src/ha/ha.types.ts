import { DeviceValue } from '../devices/devicetypes/DeviceValue.js';
import { TypedEvent } from '../utils/TypedEvent.js';

export interface HADataServiceType {
  configEmitter: TypedEvent<ConfigData>;
  dataEmitter: TypedEvent<HaData>;
  /**
   * Sent on startup when a known device is confirmed
   * @param device
   */
  deviceStarted: (serial: number) => void;
  /**
   * A device is sending a state value
   * @param evt
   * @returns
   */
  sendState: (evt: HaData) => void;
}

export interface ConfigData {}

export interface DeviceData {}

export interface EventData {
  deviceSerial: number;
  payload: string;
}

export interface HaData {
  deviceSerial: number;
  value: DeviceValue;
}
