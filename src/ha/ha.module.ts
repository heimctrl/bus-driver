import { HAAuthService } from './ha.auth.service.js';
import { HA_DATA_SERVICE } from './ha.const.js';
import { HADataService } from './ha.data.service.js';
import { ConfigModule } from '../config/config.module.js';
import { DevicesModule } from '../devices/devices.module.js';
import { LogModule } from '../log/log.module.js';
import { forwardRef, Module } from '@nestjs/common';

@Module({
  imports: [ConfigModule, LogModule, forwardRef(() => DevicesModule)],
  providers: [
    HAAuthService,
    {
      provide: HA_DATA_SERVICE,
      useClass: HADataService,
    },
  ],
  exports: [
    HAAuthService,
    {
      provide: HA_DATA_SERVICE,
      useClass: HADataService,
    },
  ],
})
export class HAModule {}
