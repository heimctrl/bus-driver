import * as net from 'net';

import { CanEventEmitter, CanService } from './can.types.js';
import { LogService } from '../log/log.service.js';
import { Queue } from '../utils/Queue.js';
import { Injectable } from '@nestjs/common';

export interface Message {
  ts_sec?: number;
  ts_usec?: number;
  id: number;
  data: Buffer;
  ext: boolean;
  rtr: boolean;
}

const serverHost = '192.168.1.224';
const serverPort = 29536;

@Injectable()
export class SocketCanDService extends CanEventEmitter implements CanService {
  private log: LogService;

  private isConnected: Promise<boolean> | boolean = false;
  private client: net.Socket;

  constructor(log: LogService) {
    super();
    this.log = log.getPrefixedLogger('[CAN_D]');
    this.log.info('Service started.');

    this.client = new net.Socket();
    this.connect().catch((err) => this.log.error('Connect error:', err));
  }

  private async ensureClient() {
    await this.isConnected;
  }

  private async connect() {
    if (this.client.connecting) {
      return;
    }

    this.client.on('error', (err) => {
      this.log.error('Connection error:', err);
      this.emit('error', err);
    });

    this.client.connect(serverPort, serverHost, () => {
      this.log.info('Connected to socketcand service.');
    });

    this.isConnected = new Promise<boolean>((resolve) => {
      const handleConnection = (data: Buffer) => {
        this.client.off('data', handleConnection);
        const message = data.toString().trim();
        if (message === '< hi >') {
          resolve(true);
        } else {
          this.log.error('Unexpected response:', message);
          this.client.destroy();
          this.emit('error', new Error(`Unexpected response while connecting: ${message}`));
          resolve(false);
        }
      };
      this.client.on('data', handleConnection);
    });

    await this.isConnected;

    let res = await this.request('< open can0 >');
    if (res !== '< ok >') {
      this.log.error(`Unable to open can0: ${res}`);
      this.isConnected = false;
      return;
    }

    res = await this.request('< rawmode >', true);
    if (res !== '< ok >') {
      this.log.error(`Unable to open can0: ${res}`);
      this.isConnected = false;
      return;
    }

    this.log.info('CAN bus can0 opened successfully.');

    // start receiving data frames
    this.client.on('data', this.receiveMessage);
  }

  private receiveMessage = (data: Buffer) => {
    const message = data.toString().trim();
    this.log.info(`Received: ${message}`);
    if (message.startsWith('< frame')) {
      // < frame can_id seconds.useconds [data]* >
      const frameParts = message.substring(8).split(' ');
      if (frameParts.length >= 2) {
        const [id, usecs, dataHex] = frameParts;
        const canId = parseInt(id, 16);
        const dataBuffer = Buffer.from(dataHex, 'hex');

        const msg: Message = {
          id: canId,
          ts_usec: parseFloat(usecs),
          data: dataBuffer,
          ext: true,
          rtr: false,
        };
        this.log.trace('Received Message:', msg);

        this.emit('message', msg.id, msg.data);
      }
    }
    if (message.startsWith('< error')) {
      // < error unknown command >
      this.emit('error', new Error(message.substring(8)));
    }
  };

  private async request(msg: string, expectResponse: boolean = false): Promise<string> {
    if (!(await this.isConnected)) {
      throw new Error('Not connected');
    }

    return new Promise<string>((resolve) => {
      this.client.write(`${msg}\n`);

      if (expectResponse) {
        const waitResponse = (data: Buffer) => {
          const message = data.toString().trim();
          this.log.info('### RCV', message);
          // not sure whether this can happen, we receive a frame just while waiting or a response?
          if (message.startsWith('< frame')) {
            return;
          }
          this.client.off('data', waitResponse);
          resolve(message);
        };
        this.client.on('data', waitResponse);
      }
    });
  }

  public sendMessage = (id: number, data: Buffer): boolean => {
    this.log.lazyDebug(() => [`Send Message to ${id.toString(16)}:`, data.length ? data.toString('hex') : '<NO DATA>']);

    try {
      const canId = id.toString(16).toUpperCase();
      const canDlc = data.length;

      const canData = data
        .reduce((acc: string[], b: number) => {
          acc.push(b.toString(16));
          return acc;
        }, [])
        .join(' ');

      const message = `< send ${canId} ${canDlc} ${canData}${canData ? ' ' : ''}>`;
      this.request(message).catch((e) => {
        const err: Error = e as Error;
        this.log.error('Error sending message to CAN bus:', err);
        this.emit('send error', err, id, data);
      });

      return true;
    } catch (e) {
      const err: Error = e as Error;
      this.log.error('Error sending message to CAN bus:', err);
      this.emit('send error', err, id, data);
    }
    return false;
  };
}
