import { CAN_SERVICE } from './can.const.js';
import { SocketCanService } from './can.service.js';
import { SocketCanDService } from './socketcand.service.js';
import { LogModule } from '../log/log.module.js';
import { Module } from '@nestjs/common';

// choose can service automatically
// if socketcan could not be installed then use socketcand
let canService: typeof SocketCanService | typeof SocketCanDService;
try {
  require.resolve('socketcan');
  canService = SocketCanService;
} catch (err) {
  canService = SocketCanDService;
}

@Module({
  imports: [LogModule],
  providers: [
    {
      provide: CAN_SERVICE,
      useClass: canService,
    },
  ],
  exports: [
    {
      provide: CAN_SERVICE,
      useClass: canService,
    },
  ],
})
export class CanModule {}
