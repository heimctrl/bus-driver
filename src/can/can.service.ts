import { CanEventEmitter, CanService } from './can.types.js';
import { LogService } from '../log/log.service.js';
import { Injectable } from '@nestjs/common';

export interface Message {
  ts_sec?: number;
  ts_usec?: number;
  id: number;
  data: Buffer;
  ext: boolean;
  rtr: boolean;
}

@Injectable()
export class SocketCanService extends CanEventEmitter implements CanService {
  private log: LogService;
  private channel;

  constructor(log: LogService) {
    super();

    // dynamic require since we should only get here if the optional module is installed
    const { createRawChannel } = require('socketcan');

    this.channel = createRawChannel('can0', true);

    this.channel.addListener('onMessage', this.receiveMessage);
    this.channel.disableLoopback();
    this.channel.start();

    this.log = log.getPrefixedLogger('[CAN]');
    this.log.info('Service started.');
  }

  private receiveMessage = (msg: Message) => {
    this.log.trace('Received Message:', msg);
    this.emit('message', msg.id, msg.data);
  };

  public sendMessage = (id: number, data: Buffer): boolean => {
    this.log.lazyDebug(() => [`Send Message to ${id.toString(16)}:`, data.length ? data.toString('hex') : '<NO DATA>']);

    const msg: Message = {
      id,
      data,
      ext: true,
      rtr: false,
    };

    try {
      const result = this.channel.send(msg);
      // this.log.debug(`Send result: ${result}`);
      return true;
    } catch (e) {
      const err: Error = e as Error;
      this.log.error('Error sending message to CAN bus:', err);
      // emit error
      this.emit('send error', err, id, data);
    }
    return false;
  };
}
