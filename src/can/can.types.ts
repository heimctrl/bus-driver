import { EventEmitter } from 'events';

export type OnMessageEventHandler = (id: number, data: Buffer) => void;
export type OnErrorEventHandler = (err: Error) => void;
export type OnSendErrorEventHandler = (err: Error, id: number, data: Buffer) => void;

type CanServiceEvents = {
  'message': Parameters<OnMessageEventHandler>;
  'error': Parameters<OnErrorEventHandler>;
  'send error': Parameters<OnSendErrorEventHandler>;
};

export class CanEventEmitter extends EventEmitter {
  emit<K extends keyof CanServiceEvents>(event: K, ...args: CanServiceEvents[K]): boolean {
    return super.emit(event, ...args);
  }

  on<K extends keyof CanServiceEvents>(event: K, listener: (...args: CanServiceEvents[K]) => void): this {
    return super.on(event, listener as (...args: any[]) => void);
  }
}

export interface CanService extends CanEventEmitter {
  sendMessage(id: number, data: Buffer): boolean;
}
