import { ConfigService, configFactories } from './config.service.js';
import { Module } from '@nestjs/common';

@Module({
  providers: [ConfigService, ...configFactories],
  exports: [...configFactories],
})
export class ConfigModule {}
