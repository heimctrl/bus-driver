import * as iots from 'io-ts';

const logLevelCodecInput = {
  debug: null,
  error: null,
  warn: null,
  info: null,
  trace: null,
};

export const LogLevelCodec = iots.keyof(logLevelCodecInput);

export const LogConfigCodec = iots.interface({
  level: LogLevelCodec,
});

export type LogConfig = iots.TypeOf<typeof LogConfigCodec>;

export const ServerConfigCodec = iots.interface({
  room: iots.string,
  homeassistant: iots.interface({
    url: iots.string,
    defaultDashboard: iots.string,
    mqtt: iots.interface({
      broker: iots.string,
      username: iots.string,
      password: iots.string,
    }),
  }),
});

export type ServerConfig = iots.TypeOf<typeof ServerConfigCodec>;

export const DatabaseConfigCodec = iots.interface({
  /**
   * Database file location
   */
  location: iots.union([iots.string, iots.undefined]),
});

export type DatabaseConfig = iots.TypeOf<typeof DatabaseConfigCodec>;
