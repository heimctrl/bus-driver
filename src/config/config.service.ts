import {
  DatabaseConfig,
  DatabaseConfigCodec,
  LogConfig,
  LogConfigCodec,
  ServerConfig,
  ServerConfigCodec,
} from './config.codecs.js';
import { CONFIGS } from '../enums/config-enums.js';
import { Injectable, Provider } from '@nestjs/common';
import config from 'config';
import { isRight } from 'fp-ts/lib/Either.js';

const codecs = {
  [CONFIGS.log]: LogConfigCodec,
  [CONFIGS.server]: ServerConfigCodec,
  [CONFIGS.database]: DatabaseConfigCodec,
};

@Injectable()
export class ConfigService {
  private static isConfigValid<TSetting extends CONFIGS>(setting: TSetting) {
    const foundConfig = config.get(setting);
    const foundCodec = codecs[setting];
    const result = foundCodec.decode(foundConfig);
    return isRight(result as any);
  }

  public async get(setting: CONFIGS.log): Promise<LogConfig>;
  public async get(setting: CONFIGS.server): Promise<ServerConfig>;
  public async get(setting: CONFIGS.database): Promise<DatabaseConfig>;
  public async get(setting: CONFIGS): Promise<any>;
  public async get(setting: CONFIGS): Promise<any> {
    const foundConfig = config.get(setting);
    const foundCodec = codecs[setting];
    const result = foundCodec.decode(foundConfig);
    const isValid = isRight(result as any);
    // let isValid = false;
    // switch (setting) {
    //   case CONFIGS.log: {
    //     const foundCodec = codecs[setting];
    //     const result = foundCodec.decode(foundConfig);
    //     isValid = isRight(result);
    //     break;
    //   }
    //   case CONFIGS.server: {
    //     const foundCodec = codecs[setting];
    //     const result = foundCodec.decode(foundConfig);
    //     isValid = isRight(result);
    //     break;
    //   }
    //   case CONFIGS.database: {
    //     const foundCodec = codecs[setting];
    //     const result = foundCodec.decode(foundConfig);
    //     isValid = isRight(result);
    //     break;
    //   }
    // }
    if (!isValid) {
      throw new Error(`${setting} config is not valid`);
    }
    return foundConfig;
  }
}

const createProvider = (name: CONFIGS): Provider => ({
  provide: name,
  useFactory: (configService: ConfigService) => configService.get(name),
  inject: [ConfigService],
});

export const configFactories = Object.values(CONFIGS).map((setting) => createProvider(setting));
