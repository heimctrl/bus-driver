export class Queue {
  private queue: (() => Promise<any>)[];
  private processing: boolean;

  constructor() {
    this.queue = [];
    this.processing = false;
  }

  async process<T>(asyncFunction: () => Promise<T>): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      this.queue.push(async () => {
        try {
          const result = await asyncFunction();
          resolve(result);
        } catch (error) {
          reject(error);
        }
      });

      if (!this.processing) {
        this.processing = true;
        this.processNext();
      }
    });
  }

  private async processNext(): Promise<void> {
    if (this.queue.length > 0) {
      const fn = this.queue.shift();
      if (fn) {
        await fn();
      }
      return this.processNext();
    } else {
      this.processing = false;
    }
  }
}
