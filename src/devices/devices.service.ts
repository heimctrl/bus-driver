import { MessageCommand, MESSAGE_CMD_VALUE_HIGH, MESSAGE_CMD_VALUE_LOW } from './constants/MessageCommand.js';
import { DevicesRepository } from './devices.repository.js';
import {
  createFragmentedHeader,
  FragmentedHeader,
  FragmentedHeaderType,
  parseFragmentedHeader,
} from './messages/FragmentedMessage.js';
import { parseNatOfferRequest, parseStartInfo } from './messages/index.js';
import { Message, MessageWithSerial } from './messages/Message.js';
import { parsePing } from './messages/Ping.js';
import { parseValueMessage } from './messages/ValueMessage.js';
import { Device } from './models/Device.js';
import { EVENT_NEXT } from './states/commonEvents.js';
import { CanEvent } from './states/DeviceStateMachine/CanEvent.js';
import { DeviceStateContext } from './states/DeviceStateMachine/DeviceStateContext.js';
import {
  EVENT_MSG_OFFER_CONFIRM,
  EVENT_MSG_OFFER_REQUEST,
  EVENT_MSG_VALUE_FROM_DEVICE,
  EVENT_MSG_START_INFO,
  EVENT_MSG_PING,
  EVENT_MSG_VALUE_TO_DEVICE,
} from './states/DeviceStateMachine/events.js';
import { ServerEvent } from './states/DeviceStateMachine/ServerEvent.js';
import { DeviceStateMachine } from './states/DeviceStateMachine.js';
import { ServerStateMachine, ServerStates } from './states/ServerStateMachine.js';
import { StartupStateMachine, StartupStates } from './states/StartupStateMachine.js';
import { createBroadcastId, createId, MessageDir, MessagePkg } from './utils/createId.js';
import { CanId, parseId } from './utils/parseId.js';
import { CAN_SERVICE } from '../can/can.const.js';
import { CanService, OnMessageEventHandler } from '../can/can.types.js';
import { HA_DATA_SERVICE } from '../ha/ha.const.js';
import { HaData, HADataServiceType } from '../ha/ha.types.js';
import { LogService } from '../log/log.service.js';
import { toHex } from '../log/utils/toHex.js';
import { Inject, Injectable } from '@nestjs/common';
import { interpret, InterpreterFrom, State } from 'xstate';

interface DeviceContext {
  /**
   * Serial number of the device for identification. 32bit integer
   */
  serial: number;
  /**
   * Current state machine handling device communication.
   */
  state: InterpreterFrom<typeof DeviceStateMachine>;
}

interface FragmentedMessage {
  rcvDate: Date;
  command: MessageCommand;
  /**
   * Total expected data size
   */
  size: number;
  /**
   * Length of data already received
   */
  length: number;
  parts: Buffer[];
}

const EMPTY_MESSAGE = Buffer.alloc(0, 0, 'binary');

@Injectable()
export class DevicesService {
  private log: LogService;
  private state: InterpreterFrom<typeof ServerStateMachine>;
  private startupState: InterpreterFrom<typeof StartupStateMachine> | null = null;
  private deviceContext: Record<number, DeviceContext> = {};
  // list of fragmented messages being received, indexed by device NAT
  private fragmentedMessages: Record<number, FragmentedMessage> = {};

  constructor(
    log: LogService,
    private db: DevicesRepository,
    @Inject(CAN_SERVICE) private can: CanService,
    @Inject(HA_DATA_SERVICE) private haDataService: HADataServiceType,
  ) {
    this.log = log.getPrefixedLogger('[DEVICES]');

    this.state = interpret(ServerStateMachine).onTransition(this.onServerStateTransition).start();

    this.can.addListener('message', this.processCanMessage);
    this.haDataService.dataEmitter.on(this.processHaData);
    this.log.info('Service started.');
  }

  private onServerStateTransition = async (state: { value: any }): Promise<void> => {
    switch (state.value) {
      case ServerStates.Initialize:
        await this.db.initialize();
        this.state.send(EVENT_NEXT);
        break;
      case ServerStates.Startup:
        this.log.info('Starting devices');
        this.startupState = interpret(StartupStateMachine).onTransition(this.onStartupStateTransition).start();
        this.startupState.onDone(() => this.state.send(EVENT_NEXT));
        break;
      case ServerStates.Running:
        this.log.info('Entering normal running state');
        break;
    }
  };

  /**
   * On startup: check all known devices whether they respond under  their NAT
   */
  private onStartupStateTransition = async (state: { value: any }): Promise<void> => {
    switch (state.value) {
      case StartupStates.DevicesOffline:
        this.log.info('Set devices offline');
        // tell all extensions to stop sending messages onto the bus
        this.can.sendMessage(createBroadcastId(MessageCommand.SetOffline), EMPTY_MESSAGE);
        // twice with 50ms delay
        await new Promise((resolve) => setTimeout(resolve, 50));
        this.can.sendMessage(createBroadcastId(MessageCommand.SetOffline), EMPTY_MESSAGE);
        this.startupState?.send(EVENT_NEXT);
        break;
      case StartupStates.ConfirmDevices:
        this.log.info('Confirming known devices');

        for (const device of this.db.getDevices()) {
          const { serial } = device;
          const context: DeviceContext = this.getContextBySerial(serial);
          context.state.send(EVENT_MSG_OFFER_CONFIRM);
        }

        this.startupState?.send(EVENT_NEXT);
        break;
      case StartupStates.IdentifyUnknown:
        this.log.info('Identify unknown devices');

        this.can.sendMessage(createBroadcastId(MessageCommand.IdentifyUnknown), EMPTY_MESSAGE);

        break;
    }
  };

  private onDeviceStateTransition = async (state: { value: any }): Promise<void> => {};

  /**
   * Get a device context by the device serial number
   */
  private getContextBySerial(serial: number): DeviceContext {
    // find the device in our list
    let context = this.deviceContext[serial];
    if (!context) {
      const deviceContext: DeviceStateContext = {
        serial,
        log: this.log.getPrefixedLogger(`[DEVICE:${toHex(serial)}]`),
        db: this.db,
        sendMessage: this.sendMessage,
        haDataService: this.haDataService,
      };
      const state = interpret(DeviceStateMachine.withContext(deviceContext))
        .onTransition(this.onDeviceStateTransition)
        .start();

      context = {
        serial,
        state,
      };
      this.deviceContext[serial] = context;
    }
    return context;
  }

  private processCanMessage: OnMessageEventHandler = async (id: number, data: Buffer): Promise<void> => {
    const parsedId: CanId = parseId(id);

    if (parsedId.dir === MessageDir.FromExtension) {
      this.sendMessageToDeviceState(parsedId, data);
    }
  };

  private sendMessageToDeviceState = (parsedId: CanId, data: Buffer): void => {
    const { dir, pkg, nat, cmd } = parsedId;
    this.log.debug(`Receive message: ${dir} ${pkg} 0x${toHex(nat)} 0x${toHex(cmd)}`);
    this.log.lazyDebug(() => [`data: ${data.toString('hex')}`]);

    try {
      let event: CanEvent<Message | MessageWithSerial> | undefined;

      switch (cmd) {
        case MessageCommand.FragmentedHeader:
          this.startFragmentedMessage(parsedId, parseFragmentedHeader(data));
          break;
        case MessageCommand.FragmentedData:
          this.continueFragmentedMessage(parsedId, data);
          break;
        case MessageCommand.NatOfferRequest:
          event = {
            type: EVENT_MSG_OFFER_REQUEST,
            id: parsedId,
            msg: parseNatOfferRequest(data),
          };
          break;
        case MessageCommand.StartInfo:
          event = {
            type: EVENT_MSG_START_INFO,
            id: parsedId,
            msg: parseStartInfo(data),
          };
          break;
        case MessageCommand.Ping:
          event = {
            type: EVENT_MSG_PING,
            id: parsedId,
            msg: parsePing(data),
          };
          break;
        default:
          // process value updates
          if (cmd >= MESSAGE_CMD_VALUE_LOW && cmd <= MESSAGE_CMD_VALUE_HIGH) {
            event = {
              type: EVENT_MSG_VALUE_FROM_DEVICE,
              id: parsedId,
              msg: parseValueMessage(data),
            };
          }
          break;
      }

      if (event) {
        let serial = (event as CanEvent<MessageWithSerial>).msg.serialNumber;
        if (!serial) {
          const device: Device | null = this.db.findByNat(nat);
          if (device) serial = device.serial;
        }
        if (!serial) {
          this.log.error(`Message 0x${toHex(cmd)} from unknown device 0x${toHex(nat)}`);
          return;
        }

        const context: DeviceContext = this.getContextBySerial(serial);
        this.log.debug(
          `Send to state machine "${event.type}" ${toHex(context.serial)} (${JSON.stringify(
            context.state.state.value,
          )})`,
        );
        context.state.send(event);
      }
    } catch (e) {
      this.log.error(`Error processing message from 0x${toHex(nat)} (cmd: 0x${toHex(cmd)}):`, e);
    }
  };

  private startFragmentedMessage(parsedId: CanId, header: FragmentedHeader) {
    const { nat } = parsedId;
    const { command, size } = header;

    this.log.debug(`Start fragmented message for NAT 0x${toHex(nat)}`);
    this.fragmentedMessages[nat] = {
      rcvDate: new Date(),
      command,
      size,
      length: 0,
      parts: [],
    };
  }

  private continueFragmentedMessage(parsedId: CanId, data: Buffer) {
    const { nat } = parsedId;

    const message = this.fragmentedMessages[nat];
    if (!message) {
      this.log.error(`Received unexpected fragmented data for NAT 0x${toHex(nat)}`);
      return;
    }

    this.log.debug(
      `Fragmented data ${message.length}-${message.length + data.length} of ${message.size} for NAT 0x${toHex(nat)}`,
    );
    message.length += data.length;
    message.parts.push(data);

    if (message.length >= message.size) {
      const id: CanId = {
        dir: MessageDir.FromExtension,
        pkg: MessagePkg.Normal,
        nat,
        cmd: message.command,
      };
      const fullData: Buffer = Buffer.concat(message.parts);
      this.sendMessageToDeviceState(id, fullData);
    }
  }

  private sendMessage = (nat: number, cmd: MessageCommand, data: Buffer): boolean => {
    if (data.length <= 8) {
      return this.can.sendMessage(createId(MessageDir.FromServer, MessagePkg.Normal, nat, cmd), data);
    }

    this.log.debug(`Sending fragmented message to 0x${toHex(nat)} (${data.length} bytes)`);

    let id: number = createId(MessageDir.FromServer, MessagePkg.Fragmented, nat, MessageCommand.FragmentedHeader);
    this.log.trace(`Sending fragmented header to 0x${toHex(id)}`);
    let result: boolean = this.can.sendMessage(
      id,
      createFragmentedHeader({ msgType: FragmentedHeaderType, command: cmd, size: data.length }),
    );
    if (!result) {
      return result;
    }

    let offset = 0;
    id = createId(MessageDir.FromServer, MessagePkg.Fragmented, nat, MessageCommand.FragmentedData);
    do {
      const len = Math.min(data.length - offset, 8);
      const messageData: Buffer = data.slice(offset, offset + len);
      this.log.trace(`Sending fragmented data at offset ${offset} to ${toHex(id)}`);
      result &&= this.can.sendMessage(id, messageData);
      offset += 8;
    } while (offset < data.length);

    return result;
  };

  private processHaData = (data: HaData): void => {
    const { deviceSerial, value } = data;
    const context: DeviceContext = this.getContextBySerial(deviceSerial);
    const event: ServerEvent = {
      type: EVENT_MSG_VALUE_TO_DEVICE,
      serial: deviceSerial,
      value,
    };
    context.state.send(event);
  };
}
