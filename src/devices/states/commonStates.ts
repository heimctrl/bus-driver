/**
 * Common final event in a state machine
 */
export const STATE_DONE = 'done';
