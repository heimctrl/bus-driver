import { EVENT_NEXT } from './commonEvents.js';
import { STATE_DONE } from './commonStates.js';
import { createMachine } from 'xstate';

export enum ServerStates {
  Initialize = 'initialize',
  Startup = 'startup',
  Running = 'running',
}

/**
 * Keeps track of the current server state
 */
export const ServerStateMachine = createMachine({
  predictableActionArguments: true,
  initial: ServerStates.Initialize,
  states: {
    [ServerStates.Initialize]: {
      on: {
        [EVENT_NEXT]: ServerStates.Startup,
      },
    },
    [ServerStates.Startup]: {
      on: {
        [EVENT_NEXT]: ServerStates.Running,
      },
    },
    [ServerStates.Running]: {
      on: {
        [EVENT_NEXT]: STATE_DONE,
      },
    },
    [STATE_DONE]: {
      type: 'final',
    },
  },
});
