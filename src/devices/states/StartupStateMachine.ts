import { EVENT_NEXT } from './commonEvents.js';
import { STATE_DONE } from './commonStates.js';
import { createMachine } from 'xstate';

export enum StartupStates {
  DevicesOffline = 'devices_offline',
  ConfirmDevices = 'confirm_devices',
  IdentifyUnknown = 'identify_unknown',
}

/**
 * Executed on server startup to discover existing and new devices
 */
export const StartupStateMachine = createMachine({
  predictableActionArguments: true,
  initial: StartupStates.DevicesOffline,
  states: {
    [StartupStates.DevicesOffline]: {
      on: {
        [EVENT_NEXT]: StartupStates.ConfirmDevices,
      },
    },
    [StartupStates.ConfirmDevices]: {
      on: {
        [EVENT_NEXT]: StartupStates.IdentifyUnknown,
      },
    },
    [StartupStates.IdentifyUnknown]: {
      on: {
        [EVENT_NEXT]: STATE_DONE,
      },
    },
    [STATE_DONE]: {
      type: 'final',
    },
  },
});
