/**
 * Common event to advance a sequence
 */
export const EVENT_NEXT = 'NEXT';

export const EVENT_MESSAGE_RECEIVED = 'NEXT';
