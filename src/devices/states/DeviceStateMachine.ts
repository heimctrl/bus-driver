import { EVENT_NEXT } from './commonEvents.js';
import { DeviceStateContext } from './DeviceStateMachine/DeviceStateContext.js';
import {
  EVENT_MSG_OFFER_CONFIRM,
  EVENT_MSG_OFFER_REQUEST,
  EVENT_MSG_START_INFO,
  EVENT_MSG_UPDATE_CONFIG,
  EVENT_MSG_VALUE_FROM_DEVICE,
  EVENT_MSG_VALUE_TO_DEVICE,
  EVENT_MSG_PING,
} from './DeviceStateMachine/events.js';
import { serviceNatOfferConfirm } from './DeviceStateMachine/serviceNatOfferConfirm.js';
import { serviceNatOfferRequest } from './DeviceStateMachine/serviceNatOfferRequest.js';
import { servicePing } from './DeviceStateMachine/servicePing.js';
import { serviceProcessValue } from './DeviceStateMachine/serviceProcessValue.js';
import { serviceSendConfig } from './DeviceStateMachine/serviceSendConfig.js';
import { serviceSendValue } from './DeviceStateMachine/serviceSendValue.js';
import { serviceStartInfo } from './DeviceStateMachine/serviceStartInfo.js';
import { createMachine, EventObject, forwardTo, send } from 'xstate';

const STATE_ERROR = 'error';
const STATE_OFFER_REQUEST = 'offer_request';
const STATE_OFFER_CONFIRM = 'offer_confirm';
const STATE_START_INFO = 'start_info';
const STATE_SEND_CONFIG = 'send_config';
const STATE_RUNNING = 'running';

const SVC_OFFER_REQUEST = 'SVC_OFFER_REQUEST';
const SVC_OFFER_CONFIRM = 'SVC_OFFER_CONFIRM';
const SVC_START_INFO = 'SVC_START_INFO';
const SVC_SEND_CONFIG = 'SVC_SEND_CONFIG';
const SVC_PROCESS_VALUE = 'SVC_PROCESS_VALUE';
const SVC_SEND_VALUE = 'SVC_SEND_VALUE';
const SVC_PING = 'SVC_PING';

export const DeviceStateMachine = createMachine<DeviceStateContext>(
  {
    predictableActionArguments: true,
    initial: STATE_RUNNING,
    states: {
      [STATE_ERROR]: {
        entry: ['logError'],
        on: {
          // the device requested a new nat
          [EVENT_MSG_OFFER_REQUEST]: STATE_OFFER_REQUEST,
          // the device requested a new nat
          [EVENT_MSG_START_INFO]: STATE_START_INFO,
        },
      },
      [STATE_OFFER_REQUEST]: {
        invoke: {
          src: SVC_OFFER_REQUEST,
          onDone: STATE_OFFER_CONFIRM,
          onError: STATE_ERROR,
        },
      },
      [STATE_OFFER_CONFIRM]: {
        invoke: {
          src: SVC_OFFER_CONFIRM,
          onDone: STATE_RUNNING,
          onError: STATE_ERROR,
        },
      },
      [STATE_START_INFO]: {
        invoke: {
          id: SVC_START_INFO,
          src: serviceStartInfo,
          autoForward: true,
          onError: STATE_ERROR,
        },
        entry: send((_, e) => e, { to: SVC_START_INFO }),
        on: {
          // the device needs a new nat since nat and serial do not match
          [EVENT_MSG_OFFER_REQUEST]: STATE_OFFER_REQUEST,
          // device updated, transition to running state
          [EVENT_NEXT]: STATE_SEND_CONFIG,
        },
      },
      [STATE_SEND_CONFIG]: {
        invoke: {
          id: SVC_SEND_CONFIG,
          src: serviceSendConfig,
          autoForward: true,
          onError: STATE_ERROR,
        },
        entry: send((_, e) => e, { to: SVC_SEND_CONFIG }),
        on: {
          // the device needs a new nat since it was parked/unparked
          [EVENT_MSG_OFFER_CONFIRM]: STATE_OFFER_CONFIRM,
          // device configured, transition to running state
          [EVENT_NEXT]: STATE_RUNNING,
        },
      },
      [STATE_RUNNING]: {
        invoke: [
          {
            id: SVC_PROCESS_VALUE,
            src: SVC_PROCESS_VALUE,
            autoForward: true,
            onError: STATE_ERROR,
          },
          {
            id: SVC_PING,
            src: SVC_PING,
            autoForward: true,
            onError: STATE_ERROR,
          },
          {
            id: SVC_SEND_VALUE,
            src: SVC_SEND_VALUE,
            autoForward: true,
            onError: STATE_ERROR,
          },
          {
            id: SVC_SEND_CONFIG,
            src: SVC_SEND_CONFIG,
            autoForward: true,
            onError: STATE_ERROR,
          },
        ],
        on: {
          // used when the device is already known
          [EVENT_MSG_OFFER_CONFIRM]: STATE_OFFER_CONFIRM,
          // the device requested a new nat
          [EVENT_MSG_OFFER_REQUEST]: STATE_OFFER_REQUEST,
          // the device has started
          [EVENT_MSG_START_INFO]: STATE_START_INFO,
          [EVENT_MSG_PING]: {
            actions: forwardTo(SVC_PING),
          },
          // the sends a value
          [EVENT_MSG_VALUE_FROM_DEVICE]: {
            actions: forwardTo(SVC_PROCESS_VALUE),
          },
          [EVENT_MSG_VALUE_TO_DEVICE]: {
            actions: forwardTo(SVC_SEND_VALUE),
          },
          [EVENT_MSG_UPDATE_CONFIG]: {
            actions: forwardTo(SVC_SEND_CONFIG),
          },
        },
      },
      done: {
        type: 'final',
      },
    },
  },
  {
    actions: {
      logError: (context: DeviceStateContext, event: EventObject) => context.log.error('Device error on event:', event),
    },
    services: {
      [SVC_OFFER_REQUEST]: serviceNatOfferRequest,
      [SVC_OFFER_CONFIRM]: serviceNatOfferConfirm,
      [SVC_PROCESS_VALUE]: serviceProcessValue,
      [SVC_SEND_VALUE]: serviceSendValue,
      [SVC_SEND_CONFIG]: serviceSendConfig,
      [SVC_PING]: servicePing,
    },
  },
);
