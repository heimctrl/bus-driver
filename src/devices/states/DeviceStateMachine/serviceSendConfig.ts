import { CanEvent } from './CanEvent.js';
import { DeviceStateContext } from './DeviceStateContext.js';
import { EVENT_MSG_OFFER_CONFIRM } from './events.js';
import { toHex } from '../../../log/utils/toHex.js';
import { MessageCommand } from '../../constants/MessageCommand.js';
import { configOrDefault } from '../../devicetypes/defaults.js';
import { serializeDeviceConfig } from '../../devicetypes/DeviceConfigSerialization.js';
import { StartInfo } from '../../messages/StartInfo.js';
import { Device } from '../../models/Device.js';
import { EVENT_NEXT } from '../commonEvents.js';
import { EventObject, Receiver, Sender } from 'xstate';

/**
 * Send the device configuration and enable the device if it is parked
 * @param context Execute the SendConfig service
 * @param event
 */
export function serviceSendConfig(context: DeviceStateContext) {
  return (cb: Sender<EventObject>, onReceive: Receiver<CanEvent<StartInfo> | EventObject>) => {
    onReceive(async () => {
      const { serial, log, db, sendMessage } = context;

      log.info('state: SendConfig');

      // verify that we know about this device and that the nat is correct
      const device: Device | null = db.findBySerial(serial);
      if (!device) {
        // device not found so send a new id
        log.warn(`Device ${toHex(serial)} not found`);
        return;
      }

      if (!device.config) {
        db.updateDevice(serial, {
          config: configOrDefault(device.deviceType),
        });
      }

      // send device configuration as response
      // await sendMessage(
      //   device.natId,
      //   MessageCommand.SendConfig,
      //   serializeDeviceConfig(device.deviceType, {
      //     ...configOrDefault(device.deviceType),
      //     ...device.config,
      //   }),
      // );

      // unpark the device if it is parked since parking feature is not currently used
      let newNatId = device.natId;
      if (newNatId & 0x80) {
        // unpark the device
        newNatId &= 0x7f;
      }

      if (device.natId !== newNatId) {
        // parked or unparked
        log.info(`Device ${toHex(serial)} nat changed: ${toHex(device.natId)} => ${toHex(newNatId)}`);
        db.updateDevice(serial, {
          returnToNatId: device.natId,
          natId: newNatId,
        });
        cb({ type: EVENT_MSG_OFFER_CONFIRM });
        return;
      }

      // transition to next state
      cb({ type: EVENT_NEXT });
    });
  };
}
