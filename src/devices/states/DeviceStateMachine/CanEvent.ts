import { Message } from '../../messages/Message.js';
import { CanId } from '../../utils/parseId.js';
import { EventObject } from 'xstate';

export interface CanEvent<TMessage extends Message> extends EventObject {
  id: CanId;
  msg: TMessage;
}
