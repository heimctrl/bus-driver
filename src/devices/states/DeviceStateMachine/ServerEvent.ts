import { DeviceValue } from '../../devicetypes/DeviceValue.js';
import { EventObject } from 'xstate';

/**
 * An event received from the automation server
 */
export interface ServerEvent extends EventObject {
  serial: number;
  value: DeviceValue;
}
