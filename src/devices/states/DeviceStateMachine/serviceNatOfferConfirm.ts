import { DeviceStateContext } from './DeviceStateContext.js';
import { MessageCommand } from '../../../devices/constants/MessageCommand.js';
import { toHex } from '../../../log/utils/toHex.js';
import { createNatOfferConfirm, NatOfferConfirmType } from '../../messages/NatOfferConfirm.js';
import { Device } from '../../models/Device.js';

/**
 *
 * @param context Execute the NatOfferConfirm service
 */
export async function serviceNatOfferConfirm(context: DeviceStateContext) {
  const { serial, log, db, sendMessage } = context;

  log.info('msg: NatOfferConfirm');

  // check devices whether we already have a nat
  const device: Device | null = db.findBySerial(serial);

  if (!device) {
    // device not found so transition to error state
    log.warn(`Device ${toHex(serial)} not found`);
    return;
  }

  sendMessage(
    typeof device.returnToNatId === 'number' ? device.returnToNatId : device.natId,
    MessageCommand.NatOfferConfirm,
    createNatOfferConfirm({
      msgType: NatOfferConfirmType,
      natId: device.natId,
      serialNumber: device.serial,
    }),
  );

  // remove self-assigned nat
  if (typeof device.returnToNatId === 'number') {
    await db.updateDevice(device.serial, { returnToNatId: undefined });
  }
}
