import { MessageCommand } from '../../../devices/constants/MessageCommand.js';
import { HADataServiceType } from '../../../ha/ha.types.js';
import { LogService } from '../../../log/log.service.js';
import { DevicesRepository } from '../../devices.repository.js';

export type SendMessage = (nat: number, cmd: MessageCommand, data: Buffer) => boolean;
export type FireEvent = (nat: number, eventIndex: number) => boolean;

export interface DeviceStateContext {
  serial: number;
  log: LogService;
  db: DevicesRepository;
  sendMessage: SendMessage;
  // fireEvent: FireEvent;
  haDataService: HADataServiceType;
}
