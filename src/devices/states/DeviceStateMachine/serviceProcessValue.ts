import { CanEvent } from './CanEvent.js';
import { DeviceStateContext } from './DeviceStateContext.js';
import { toHex } from '../../../log/utils/toHex.js';
import { DeviceType } from '../../constants/DeviceType.js';
import { DeviceValue } from '../../devicetypes/DeviceValue.js';
import { deserializeDeviceValue } from '../../devicetypes/DeviceValueSerialization.js';
import { ValueMessage } from '../../messages/ValueMessage.js';
import { Device } from '../../models/Device.js';
import { EventObject, Receiver, Sender } from 'xstate';

/**
 *
 * @param context Execute the ProcessValue service
 * @param event
 */
export function serviceProcessValue(context: DeviceStateContext) {
  return (cb: Sender<EventObject>, onReceive: Receiver<CanEvent<ValueMessage> | EventObject>) => {
    onReceive(async (event: CanEvent<ValueMessage> | EventObject) => {
      const { serial, log, db, haDataService } = context;
      const { id, msg } = event as CanEvent<ValueMessage>;
      const { data } = msg;
      const { cmd } = id;

      log.info('msg: ProcessValue');

      // verify that we know about this device and that the nat is correct
      const device: Device | null = db.findBySerial(serial);
      if (!device) {
        // device not found, ignore value
        log.warn(`Device ${toHex(serial)} not found`);
        return;
      }

      // update lastSeen
      log.info('ProcessValue: Device updated');
      await db.updateDevice(serial, {
        lastSeen: new Date(),
      });

      // TODO: send value to home automation
      const value: DeviceValue = deserializeDeviceValue(device.deviceType, { cmd, data });
      log.debug('ProcessValue:', value);

      haDataService.sendState({
        deviceSerial: device.serial,
        value,
      });
    });
  };
}
