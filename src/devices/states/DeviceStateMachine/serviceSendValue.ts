import { DeviceStateContext } from './DeviceStateContext.js';
import { ServerEvent } from './ServerEvent.js';
import { toHex } from '../../../log/utils/toHex.js';
import { serializeDeviceValue } from '../../devicetypes/DeviceValueSerialization.js';
import { Device } from '../../models/Device.js';
import { EventObject, Receiver, Sender } from 'xstate';

/**
 * Send a value to a device
 * @param context
 * @param event
 */
export function serviceSendValue(context: DeviceStateContext) {
  return (cb: Sender<EventObject>, onReceive: Receiver<ServerEvent | EventObject>) => {
    onReceive(async (event: ServerEvent | EventObject) => {
      const { serial, log, db, sendMessage } = context;
      const { value } = event as ServerEvent;

      log.info('msg: SendValue');

      // verify that we know about this device and that the nat is correct
      const device: Device | null = db.findBySerial(serial);
      if (!device) {
        // device not found, ignore value
        log.warn(`Device ${toHex(serial)} not found`);
        return;
      }

      const { cmd, data } = serializeDeviceValue(device.deviceType, value);
      log.debug('SendValue:', cmd, data);

      await sendMessage(device.natId, cmd, data);
    });
  };
}
