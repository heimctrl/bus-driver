import { CanEvent } from './CanEvent.js';
import { DeviceStateContext } from './DeviceStateContext.js';
import { EVENT_MSG_OFFER_REQUEST } from './events.js';
import { toHex } from '../../../log/utils/toHex.js';
import { MessageCommand } from '../../constants/MessageCommand.js';
import { Ping, createPong } from '../../messages/index.js';
import { NatOfferRequest, NatOfferRequestType } from '../../messages/NatOfferRequest.js';
import { Device } from '../../models/Device.js';
import { EVENT_NEXT } from '../commonEvents.js';
import { EventObject, Receiver, Sender } from 'xstate';

/**
 *
 * @param context Execute the Ping service
 * @param event
 */
export function servicePing(context: DeviceStateContext) {
  return (cb: Sender<EventObject>, onReceive: Receiver<CanEvent<Ping> | EventObject>) => {
    onReceive(async (event: CanEvent<Ping> | EventObject) => {
      const { serial, log, db, sendMessage } = context;
      const { id, msg } = event as CanEvent<Ping>;
      const { serialNumber } = msg;

      log.info('msg: Ping');

      // verify that we know about this device and that the nat is correct
      const device: Device | null = db.findBySerial(serial);
      if (!device) {
        log.warn(`Device ${toHex(serial)} not found`);
        return;
      }

      if (device.natId !== id.nat) {
        // wrong nat
        log.warn(`Device ${toHex(serial)} used unexpected nat ${toHex(id.nat)} instead of ${toHex(device.natId)}`);
        const event: CanEvent<NatOfferRequest> = {
          type: EVENT_MSG_OFFER_REQUEST,
          id,
          msg: {
            msgType: NatOfferRequestType,
            deviceType: device.deviceType,
            serialNumber,
          },
        };
        cb(event);
        return;
      }

      log.info('Ping: Device updated');
      await db.updateDevice(serial, {
        lastSeen: new Date(),
      });

      // send pong as response
      await sendMessage(device.natId, MessageCommand.Pong, createPong());

      // let HA know the device is online
      context.haDataService.deviceStarted(serial);

      cb({ type: EVENT_NEXT });
    });
  };
}
