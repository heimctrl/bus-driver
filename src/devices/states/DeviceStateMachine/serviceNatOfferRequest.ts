import { CanEvent } from './CanEvent.js';
import { DeviceStateContext } from './DeviceStateContext.js';
import { toHex } from '../../../log/utils/toHex.js';
import { NatOfferRequest } from '../../messages/NatOfferRequest.js';
import { Device } from '../../models/Device.js';
import { EventObject } from 'xstate';

/**
 *
 * @param context Execute the NatOfferRequest service
 * @param event
 */
export async function serviceNatOfferRequest(
  context: DeviceStateContext,
  event: CanEvent<NatOfferRequest> | EventObject,
) {
  const { serial, log, db } = context;
  const { id, msg } = event as CanEvent<NatOfferRequest>;

  log.info('msg: NatOfferRequest');
  log.debug(`Device type: ${toHex(msg.deviceType)}`);
  // check devices whether we already have a nat
  let device: Device | null = db.findBySerial(serial);

  // make a new device
  if (!device) {
    // TODO: better way to make sure the NAT is unique
    let natId;
    do {
      // generate NAT
      natId = Math.round(Math.random() * 0x7e) + 1;
    } while (db.findByNat(natId) !== null);
    // and mark device as parked
    natId |= 0x80;
    log.info(`New NAT: ${toHex(natId)} (Device Nat: ${id.nat})`);

    device = {
      natId,
      returnToNatId: id.nat,
      serial,
      deviceType: msg.deviceType,
      lastSeen: new Date(),
    };
    await db.insertDevice(device);
  } else {
    await db.updateDevice(device.serial, { returnToNatId: id.nat });
    log.info(`Existing NAT: ${toHex(device.natId)} (Device Nat: ${id.nat})`);
  }
}
