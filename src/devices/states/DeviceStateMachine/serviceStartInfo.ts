import { CanEvent } from './CanEvent.js';
import { DeviceStateContext } from './DeviceStateContext.js';
import { EVENT_MSG_OFFER_REQUEST } from './events.js';
import { toHex } from '../../../log/utils/toHex.js';
import { MessageCommand } from '../../constants/MessageCommand.js';
import { configOrDefault } from '../../devicetypes/defaults.js';
import { serializeDeviceConfig } from '../../devicetypes/DeviceConfigSerialization.js';
import { NatOfferRequest, NatOfferRequestType } from '../../messages/NatOfferRequest.js';
import { StartInfo } from '../../messages/StartInfo.js';
import { Device } from '../../models/Device.js';
import { EVENT_NEXT } from '../commonEvents.js';
import { EventObject, Receiver, Sender } from 'xstate';

/**
 *
 * @param context Execute the StartInfo service
 * @param event
 */
export function serviceStartInfo(context: DeviceStateContext) {
  return (cb: Sender<EventObject>, onReceive: Receiver<CanEvent<StartInfo> | EventObject>) => {
    onReceive(async (event: CanEvent<StartInfo> | EventObject) => {
      const { serial, log, db, sendMessage } = context;
      const { id, msg } = event as CanEvent<StartInfo>;
      const { deviceType, serialNumber } = msg;

      log.info('msg: StartInfo');

      // verify that we know about this device and that the nat is correct
      const device: Device | null = db.findBySerial(serial);
      if (!device) {
        // device not found so send a new id
        log.warn(`Device ${toHex(serial)} not found`);
        const event: CanEvent<NatOfferRequest> = {
          type: EVENT_MSG_OFFER_REQUEST,
          id,
          msg: {
            msgType: NatOfferRequestType,
            deviceType,
            serialNumber,
          },
        };
        cb(event);
        return;
      }
      if (device.natId !== id.nat) {
        // wrong nat
        log.warn(`Device ${toHex(serial)} used unexpected nat ${toHex(id.nat)} instead of ${toHex(device.natId)}`);
        const event: CanEvent<NatOfferRequest> = {
          type: EVENT_MSG_OFFER_REQUEST,
          id,
          msg: {
            msgType: NatOfferRequestType,
            deviceType,
            serialNumber,
          },
        };
        cb(event);
        return;
      }

      // transition to next state
      log.info('StartInfo: Device updated');
      await db.updateDevice(serial, {
        firmwareVersion: msg.firmwareVersion,
        hardwareVersion: msg.hardwareVersion,
        deviceType,
        lastSeen: new Date(),
      });

      // send device configuration as response
      // await sendMessage(
      //   device.natId,
      //   MessageCommand.SendConfig,
      //   serializeDeviceConfig(deviceType, configOrDefault(deviceType, device.config)),
      // );

      context.haDataService.deviceStarted(serial);

      cb({ type: EVENT_NEXT });
    });
  };
}
