import { MessageDir, MessagePkg } from './createId.js';
import { MessageCommand } from '../constants/MessageCommand.js';

export interface CanId {
  dir: MessageDir;
  pkg: MessagePkg;
  nat: number;
  cmd: MessageCommand;
}

/**
 * Parse a 32bit CAN bus id based on this structure
 * Identifier format taken from Loxone
 * 10000.0ddlnnnn.nnnn0000.cccccccc
 * cccccccc contains the command byte.
 * nnnnnnnn is the NAT (0x01...0x7E), which is the id of the extension on the Loxone Link bus.
 *   A NAT of 0xFF is a broadcast to all extensions.
 *   Bit 7 is set for parked extensions, which have been detected by the Miniserver,
 *   but are not configured in Loxone Config.
 * dd is a direction/type of the command:
 *   00 = sent from a extension/device
 *   10 = from a extension/device, when sending a shortcut messages back to the Miniserver
 *   11 = sent from the Miniserver
 * l package type: 0 = regular package, 1 = fragmented package
 */
export function parseId(id: number): CanId {
  const dir: MessageDir = (id >> 21) & 0x03;
  const pkg: MessagePkg = (id >> 20) & 0x01;
  const nat: number = (id >> 12) & 0xff;
  const cmd: MessageCommand = id & 0xff;

  return {
    dir,
    pkg,
    nat,
    cmd,
  };
}
