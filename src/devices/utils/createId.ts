import { MessageCommand } from '../constants/MessageCommand.js';

export enum MessageDir {
  FromExtension = 0b00,
  ShortcutToServer = 0b10,
  FromServer = 0b11,
}

export enum MessagePkg {
  Normal = 0,
  Fragmented = 1,
}

/**
 * Possible Device NATs:
 * 0x00 - sent from a device with the "NAT offer request" command, because it doesn't have a device NAT yet.
 * 0x01…0x3E - Right Loxone Tree bus device (62 possible devices)
 * 0x3F - reserved for tree shortcut testing
 * 0x40 - reserved, unused
 * 0x41…0x7E - Left Loxone Tree bus device (62 possible devices)
 * 0x7F - reserved for tree shortcut testing, because 0x7F parked would be 0xFF, which is used for broadcast
 * 0x80 - reserved, unused (bit marks parked devices)
 * 0x81…0xBE - Parked right Loxone Tree bus device
 * 0xBF - reserved, to be in-sync with 0xFF
 * 0xC0 - reserved, unused
 * 0xC1…0xFE - Parked left Loxone Tree bus device
 * 0xFF - broadcast to all devices
 */
export const NAT_BROADCAST = 0xff;
export const NAT_PARKED = 0x80;

/**
 * Create a 32bit CAN bus id based on this structure
 * Identifier format taken from Loxone
 * 10000.0ddlnnnn.nnnn0000.cccccccc
 * cccccccc contains the command byte.
 * nnnnnnnn is the NAT (0x01...0x7E), which is the id of the extension on the Loxone Link bus.
 *   A NAT of 0xFF is a broadcast to all extensions.
 *   Bit 7 is set for parked extensions, which have been detected by the Miniserver,
 *   but are not configured in Loxone Config.
 * dd is a direction/type of the command:
 *   00 = sent from a extension/device
 *   10 = from a extension/device, when sending a shortcut message back to the Miniserver
 *   11 = sent from the Miniserver
 * l package type: 0 = regular package, 1 = fragmented package
 */
export function createId(dir: MessageDir, pkg: MessagePkg, nat: number, cmd: MessageCommand): number {
  return (0x1 << 28) + ((dir & 0x03) << 21) + ((pkg & 0x01) << 20) + ((nat & 0xff) << 12) + (cmd & 0xff);
}

export function createBroadcastId(command: MessageCommand) {
  return createId(MessageDir.FromServer, MessagePkg.Normal, NAT_BROADCAST, command);
}
