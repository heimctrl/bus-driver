import { DevicesRepository } from './devices.repository.js';
import { DevicesService } from './devices.service.js';
import { CanModule } from '../can/can.module.js';
import { ConfigModule } from '../config/config.module.js';
import { HAModule } from '../ha/ha.module.js';
import { LogModule } from '../log/log.module.js';
import { forwardRef, Module } from '@nestjs/common';

@Module({
  imports: [LogModule, ConfigModule, CanModule, forwardRef(() => HAModule)],
  providers: [DevicesRepository, DevicesService],
  exports: [DevicesRepository, DevicesService],
})
export class DevicesModule {}
