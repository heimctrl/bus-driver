import { EventEmitter } from 'events';
import { join } from 'path';

import { Device } from './models/Device.js';
import { CONFIGS } from '../enums/config-enums.js';
import { LogService } from '../log/log.service.js';
import { Inject, Injectable } from '@nestjs/common';
import { Low, Adapter, Memory } from 'lowdb';
import { JSONFile } from 'lowdb/node';

interface DatabaseConfig {
  /**
   * Database storage root
   */
  location: string;
}

export interface DeviceDatabase {
  devices: Device[];
}

@Injectable()
export class DevicesRepository extends EventEmitter {
  private log: LogService;
  private db: Low<DeviceDatabase>;

  constructor(
    log: LogService,
    @Inject(CONFIGS.database)
    private dbConfig: DatabaseConfig,
  ) {
    super();
    this.log = log.getPrefixedLogger('[DEVICE_REPO]');

    let adapter: Adapter<DeviceDatabase>;
    if (!this.dbConfig.location) {
      this.log.warn('No root path for the database has been configured, using InMemory database');
      adapter = new Memory<DeviceDatabase>();
    } else {
      const dbPath = join(this.dbConfig.location, 'devices.json');
      this.log.info(`Using device database at ${dbPath}`);
      adapter = new JSONFile<DeviceDatabase>(dbPath);
    }
    this.db = new Low<DeviceDatabase>(adapter);

    this.log.info('Service started.');
  }

  public async initialize() {
    await this.db.read();

    // initialize data in case the db did not exist
    const isNewDB = !this.db.data || !this.db.data.devices;
    this.db.data ||= { devices: [] };
    if (isNewDB) {
      // create the database file if it didn't exist
      await this.db.write();
    }

    this.log.info(`Loaded ${this.db.data.devices.length} known devices`);
  }

  public getDevices(): Device[] {
    return this.db.data?.devices || [];
  }

  public findBySerial(serial: number): Device | null {
    if (!this.db.data) return null;
    for (const device of this.db.data.devices) {
      if (device.serial === serial) {
        return device;
      }
    }
    return null;
  }

  public findByNat(nat: number): Device | null {
    if (!this.db.data) return null;

    // ignore parked flag
    nat &= 0x7f;

    for (const device of this.db.data.devices) {
      if ((device.natId & 0x7f) === nat) {
        return device;
      }
    }
    return null;
  }

  public async insertDevice(device: Device): Promise<Device> {
    this.db.data?.devices.push(device);
    await this.db.write();
    this.emit('add', device);
    return device;
  }

  public async updateDevice(serial: number, update: Partial<Device>): Promise<Device | null> {
    let device: Device | null = this.findBySerial(serial);
    if (!device) return null;
    device = Object.assign(device, update);
    await this.db.write();
    this.emit('update', device);
    return device;
  }

  public async removeDevice(device: Device): Promise<void> {
    const idx: number | undefined = this.db.data?.devices.indexOf(device);
    if (!idx) return;
    this.db.data?.devices.splice(idx, 1);
    await this.db.write();
    this.emit('remove', device);
  }
}
