import { MessageWithSerial } from './Message.js';

export interface Ping extends MessageWithSerial {
  serialNumber: number;
}

export const PingType = 'Ping';

/**
 * Parse the Ping message
 * @param data - 0:4 device serial
 */
export function parsePing(data: Buffer): Ping {
  const serialNumber = data.readUInt32LE(0);
  return {
    msgType: PingType,
    serialNumber,
  };
}
