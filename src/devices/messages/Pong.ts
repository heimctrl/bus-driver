export const PongType = 'Pong';

/**
 * Create a Pong message
 * @param message {Pong}
 */
export function createPong(): Buffer {
  return Buffer.alloc(0);
}
