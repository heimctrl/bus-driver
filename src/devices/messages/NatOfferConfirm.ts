import { MessageWithSerial } from './Message.js';

export interface NatOfferConfirm extends MessageWithSerial {
  natId: number;
  serialNumber: number;
}

export const NatOfferConfirmType = 'NatOfferConfirm';

/**
 * Create a NatOfferConfirm message
 * @param message {NatOfferConfirm}
 */
export function createNatOfferConfirm(message: NatOfferConfirm): Buffer {
  const data: Buffer = Buffer.alloc(8);
  data.writeUInt8(message.natId, 0);
  data.writeUInt32LE(message.serialNumber, 4);
  return data;
}
