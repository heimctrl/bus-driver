import { MessageWithSerial } from './Message.js';
import { DeviceType } from '../constants/DeviceType.js';

export interface NatOfferRequest extends MessageWithSerial {
  deviceType: DeviceType;
  serialNumber: number;
}

export const NatOfferRequestType = 'NatOfferRequest';

/**
 * Parse the NatOfferRequest message
 * @param data - 0:2 reserved, 2:2 device type, 4:4 device serial
 */
export function parseNatOfferRequest(data: Buffer): NatOfferRequest {
  const deviceType = data.readUInt16LE(2) as DeviceType;
  const serialNumber = data.readUInt32LE(4);
  return {
    msgType: NatOfferRequestType,
    deviceType,
    serialNumber,
  };
}
