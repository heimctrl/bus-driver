import { Message } from './Message.js';
import { MessageCommand } from '../constants/MessageCommand.js';

export interface FragmentedHeader extends Message {
  command: MessageCommand;
  size: number;
}

export const FragmentedHeaderType = 'FragmentedHeader';

/**
 * Create the FragmentedHeader message
 * @param data - 0:1 command, 1:2 size
 */
export function createFragmentedHeader(message: FragmentedHeader): Buffer {
  const data: Buffer = Buffer.alloc(3);
  data.writeUInt8(message.command, 0);
  data.writeUInt16LE(message.size, 1);
  return data;
}

/**
 * Parse the FragmentedHeader message
 * @param data - 0:1 command, 1:2 size
 */
export function parseFragmentedHeader(data: Buffer): FragmentedHeader {
  const command = data.readUInt8(0) as MessageCommand;
  const size = data.readUInt16LE(1);
  return {
    msgType: FragmentedHeaderType,
    command,
    size,
  };
}
