export interface Message {
  msgType: string;
}

export interface MessageWithSerial extends Message {
  serialNumber: number;
}
