import { Message } from './Message.js';
import { MessageCommand } from '../constants/MessageCommand.js';

/**
 * This just wraps the raw message so it can be parsed device specifically
 */
export interface ValueMessage extends Message {
  data: Buffer;
}

export const ValueMessageType = 'ValueMessage';

/**
 * Create the ValueMessage message
 * @param data
 */
export function createValueMessage(message: ValueMessage): Buffer {
  return message.data;
}

/**
 * Parse the ValueMessage message
 */
export function parseValueMessage(data: Buffer): ValueMessage {
  return {
    msgType: ValueMessageType,
    data,
  };
}
