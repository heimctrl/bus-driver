import { MessageWithSerial } from './Message.js';
import { DeviceType } from '../constants/DeviceType.js';

export interface StartInfo extends MessageWithSerial {
  deviceType: DeviceType;
  /**
   * Byte
   */
  hardwareVersion: number;
  /**
   * Short: major.minor (first byte.second byte)
   */
  firmwareVersion: number;
  serialNumber: number;
}

export const StartInfoType = 'StartInfo';

/**
 * Parse the StartInfo message
 * @param data - 0:2 reserved, 2:2 firmware version, 4:4 device serial
 */
export function parseStartInfo(data: Buffer): StartInfo {
  const deviceType = data.readUInt16LE(0);
  const hardwareVersion = data.readUInt8(2);
  const firmwareVersion = data.readUInt16LE(3);
  const serialNumber = data.readUInt32LE(5);
  return {
    msgType: StartInfoType,
    deviceType,
    hardwareVersion,
    firmwareVersion,
    serialNumber,
  };
}
