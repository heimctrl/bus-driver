export enum MessageReason {
  /**
   * Sent sometimes, if there is no reason. Probably a bug in some devices.
   */
  Undefined = 0x00,
  /**
   * Server is starting
   */
  ServerStart = 0x01,
  /**
   * Sent by an extension in 'Start Info' in the startup phase, after the server was rebooted
   */
  Pairing = 0x02,
  /**
   * Sent by the server every 15 minutes to check the connection of extensions/devices
   */
  AliveRequested = 0x03,
  /**
   * Reconnect
   */
  Reconnect = 0x04,
  /**
   * Sent by the extension/device in response to the Alive command or when sending an Alive packet at a regular interval.
   */
  AlivePackage = 0x05,
  /**
   * Reconnect (broadcast)
   */
  ReconnectBroadcast = 0x06,
  /**
   * Sent by extensions after a power failure.
   */
  PowerOnReset = 0x20,
  /**
   * Device was in standby mode
   */
  StandbyReset = 0x21,
  /**
   * Watchdog task triggered a reset
   */
  WatchdogReset = 0x22,
  /**
   * Software reset triggered
   */
  SoftwareReset = 0x23,
  /**
   * Reset via triggered via the NRST pin
   */
  PinReset = 0x24,
  /**
   * A time based watchdog triggered
   */
  WindowWatchdogReset = 0x25,
  /**
   * Wakeup from low power mode
   */
  LowPowerReset = 0x26,
}
