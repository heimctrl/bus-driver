/**
 * 0x00...0x7F: typically common commands shared by all devices
 *   0x80...0x8F: regular value updates
 * 0xF0...0xFF: device detection, NAT translation and handling of fragmented packages
 */
export enum MessageCommand {
  None = 0x00,
  VersionRequest = 0x01,
  /**
   * 2:2 - firmware version
   * 4:4 - device serial
   */
  StartInfo = 0x02,
  VersionInfo = 0x03,
  Ping = 0x05,
  Pong = 0x06,
  SetOffline = 0x07,
  TimeSync = 0x0c,
  SendConfig = 0x11,
  /**
   * String with log error messages from the device
   */
  Logging = 0x13,
  FragmentedHeader = 0xf0,
  FragmentedData = 0xf1,
  FirmwareUpdate = 0xf3,
  IdentifyUnknown = 0xf4,
  SearchDeviceRequest = 0xfb,
  SearchDeviceResponse = 0xfc,
  /**
   * 1:1 - NAT
   * 4:4 - device serial
   */
  NatOfferConfirm = 0xfd,
  /**
   * 0:2 - reserved
   * 2:2 - device type
   * 4:4 - device serial
   */
  NatOfferRequest = 0xfe,
  // send to/from devices to update values
  DigitalValue = 0x80,
  AnalogValue = 0x81,
  RgbwValue = 0x82,
  FrequencyValue = 0x85,
}

export const MESSAGE_CMD_VALUE_LOW = 0x80;
export const MESSAGE_CMD_VALUE_HIGH = 0x8f;
