import { DeviceConfig } from './DeviceConfig.js';
import { DeviceValue } from './DeviceValue.js';
import { pwmDimmerConfigDefault } from './PWMDimmer/PWMDimmerConfig.js';
import { pwmDimmerValueDefault } from './PWMDimmer/PWMDimmerValue.js';
import { switchConfigDefault } from './Switch/SwitchConfig.js';
import { switchValueDefault } from './Switch/SwitchValue.js';
import { DeviceType } from '../constants/DeviceType.js';

export const deviceConfigDefault: Record<DeviceType, DeviceConfig> = {
  [DeviceType.SwitchCtrl]: switchConfigDefault,
  [DeviceType.PWMDimmerCtrl]: pwmDimmerConfigDefault,
};

export const deviceValueDefault: Record<DeviceType, DeviceValue> = {
  [DeviceType.SwitchCtrl]: switchValueDefault,
  [DeviceType.PWMDimmerCtrl]: pwmDimmerValueDefault,
};

export function configOrDefault(deviceType: DeviceType, config?: DeviceConfig): DeviceConfig {
  return config || deviceConfigDefault[deviceType] || { version: 0 };
}

export function valueOrDefault(deviceType: DeviceType, value?: DeviceValue): DeviceValue {
  return value || deviceValueDefault[deviceType] || { version: 0 };
}
