import { MessageCommand } from '../../constants/MessageCommand.js';
import { CommandDetector, DeviceValue, DeviceValuePacket, ValueDeserializer, ValueSerializer } from '../DeviceValue.js';

export interface PWMDimmerValue extends DeviceValue {
  /**
   * brightness of channel A in % (0-100)
   */
  channelALevel: number;
  /**
   * brightness of channel B in % (0-100)
   */
  channelBLevel: number;
}

export const pwmDimmerValueDefault: PWMDimmerValue = {
  version: 1,
  channelALevel: 0,
  channelBLevel: 0,
};

export const parsePWMDimmerValue: ValueDeserializer = (packet: DeviceValuePacket): PWMDimmerValue => {
  const { data } = packet;
  return {
    version: data.readUInt8(0),
    channelALevel: data.readUInt8(1),
    channelBLevel: data.readUInt8(2),
  };
};

export const createPWMDimmerValue: ValueSerializer = (deviceValue: DeviceValue): DeviceValuePacket => {
  const value: PWMDimmerValue = deviceValue as PWMDimmerValue;
  const data: Buffer = Buffer.allocUnsafe(3);
  data.writeUInt8(value.version, 0);
  data.writeUInt8(value.channelALevel, 1);
  data.writeUInt8(value.channelBLevel, 2);

  return {
    cmd: MessageCommand.DigitalValue,
    data,
  };
};

export const detectPWMDimmerCommand: CommandDetector = (value: DeviceValue): MessageCommand => {
  return MessageCommand.DigitalValue;
};
