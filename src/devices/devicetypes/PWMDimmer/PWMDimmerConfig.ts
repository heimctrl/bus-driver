import { ConfigDeserializer, ConfigSerializer, DeviceConfig } from '../DeviceConfig.js';

export interface PWMDimmerConfig extends DeviceConfig {
  /**
   * brightness of channel A in % after restart (0-101), 101% - retain last value
   */
  channelALevelDefault: number;
  /**
   * how quickly to fade brightness changes (seconds per 100%)
   */
  channelAFadeRate: number;
  /**
   * brightness of channel B in % after restart (0-101), 101% - retain last value
   */
  channelBLevelDefault: number;
  /**
   * how quickly to fade brightness changes (seconds per 100%)
   */
  channelBFadeRate: number;
}

/**
 * The device expects 0.1 second fade rate values
 */
const FADE_RATE_MULTIPLIER = 10;

export const pwmDimmerConfigDefault: PWMDimmerConfig = {
  version: 1,
  channelALevelDefault: 0,
  channelAFadeRate: 0,
  channelBLevelDefault: 0,
  channelBFadeRate: 0,
};

export const parsePWMDimmerConfig: ConfigDeserializer = (version: number, data: Buffer): PWMDimmerConfig => {
  return {
    version,
    channelALevelDefault: data.readUInt8(0),
    channelAFadeRate: data.readUInt8(1) / FADE_RATE_MULTIPLIER,
    channelBLevelDefault: data.readUInt8(2),
    channelBFadeRate: data.readUInt8(3) / FADE_RATE_MULTIPLIER,
  };
};

export const createPWMDimmerConfig: ConfigSerializer = (deviceConfig: DeviceConfig): Buffer => {
  const config: PWMDimmerConfig = deviceConfig as PWMDimmerConfig;
  const data: Buffer = Buffer.allocUnsafe(4);
  data.writeUInt8(config.channelALevelDefault, 0);
  data.writeUInt8(Math.round(config.channelAFadeRate * FADE_RATE_MULTIPLIER), 1);
  data.writeUInt8(config.channelBLevelDefault, 2);
  data.writeUInt8(Math.round(config.channelBFadeRate * FADE_RATE_MULTIPLIER), 3);
  return data;
};
