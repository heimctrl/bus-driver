import { ConfigDeserializer, ConfigSerializer, DeviceConfig } from '../DeviceConfig.js';

export interface SwitchConfig extends DeviceConfig {
  debounceMillis: number;
  longPressMillis: number;
  events: SwitchEventConfig[];
}

export interface SwitchEventConfig {
  // byte 0:1
  enabled: boolean;
  // byte 0:3
  pin: SwitchPin;
  // byte 0:3 - 0: single, 1: double, 2: triple, 3: long, 4: hold-release
  type: SwitchEventType;
  // byte 0:1 - 0: low, 1: high
  on_state: boolean;
  // byte 1:1 - 0: off, 1: pullup enabled
  pullup: boolean;
  // byte 1:1 - 0: off, 1: debounce enabled
  debounce: boolean;
}

export enum SwitchEventType {
  Single = 0,
  Double = 1,
  Triple = 2,
  Long = 3,
  HoldRelease = 4,
}

export type SwitchPin = 0 | 1 | 2 | 3 | 4 | 5;

const MILLIS_FACTOR = 10;
const LONG_PRESS_FACTOR = 100;
const BYTES_PER_EVENT = 2;
const MAX_EVENTS = 6;

export const switchConfigDefault: SwitchConfig = {
  version: 1,
  debounceMillis: 0,
  longPressMillis: 0,
  events: [],
};

export const parseSwitchConfig: ConfigDeserializer = (version: number, data: Buffer): SwitchConfig => {
  const numEvents = Math.min(data.readUInt8(1), MAX_EVENTS);
  const events: SwitchEventConfig[] = [];
  for (let idx = 0; idx < numEvents; idx++) {
    const offset: number = 2 + idx * 2;

    const byte0 = data.readUInt8(offset);
    const byte1 = data.readUInt8(offset + 1);

    const event: SwitchEventConfig = {
      enabled: (byte0 & 0x01) !== 0,
      pin: ((byte0 >> 1) & 0x07) as SwitchPin,
      type: ((byte0 >> 4) & 0x07) as SwitchEventType,
      on_state: ((byte0 >> 7) & 0x01) !== 0,
      pullup: (byte1 & 0x01) !== 0,
      debounce: ((byte1 >> 1) & 0x01) !== 0,
    };
    events.push(event);
  }
  return {
    version,
    // how much time to use for smoothing out button presses for debouncing (value * 10ms)
    debounceMillis: (data.readUInt8(0) & 0x0f) * MILLIS_FACTOR,
    // how long to hold at least to register a long press (value * 100ms)
    longPressMillis: data.readUInt8(0) >> (4 * LONG_PRESS_FACTOR),
    events,
  };
};

export const createSwitchConfig: ConfigSerializer = (deviceConfig: DeviceConfig): Buffer => {
  const config: SwitchConfig = deviceConfig as SwitchConfig;
  const numEvents = Math.min(config.events?.length || 0, MAX_EVENTS);
  const data: Buffer = Buffer.allocUnsafe(2 + numEvents * BYTES_PER_EVENT);

  let offset: number = 0;
  const debounceMillis: number = (config.debounceMillis / MILLIS_FACTOR) & 0x0f;
  const longPressMillis = ((config.longPressMillis / LONG_PRESS_FACTOR) & 0x0f) << 4;
  data.writeUInt8(debounceMillis + longPressMillis, offset++);

  data.writeUInt8(numEvents, offset++);
  for (let idx = 0; idx < numEvents; idx++) {
    const event: SwitchEventConfig = config.events[idx];

    let byte0: number = event.enabled ? 1 : 0;
    byte0 += (event.pin & 0x07) << 1;
    byte0 += (event.type & 0x0) << 4;
    byte0 += (event.on_state ? 1 : 0) << 7;
    let byte1 = event.pullup ? 1 : 0;
    byte1 += (event.debounce ? 1 : 0) << 1;

    data.writeUInt8(byte0, offset++);
    data.writeUInt8(byte1, offset++);
  }
  return data;
};
