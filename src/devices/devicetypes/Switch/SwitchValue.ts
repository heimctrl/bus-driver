import { MessageCommand } from '../../constants/MessageCommand.js';
import { DeviceValue, DeviceValuePacket, ValueDeserializer, ValueSerializer } from '../DeviceValue.js';

export interface SwitchValue extends DeviceValue {
  event: number;
}

export const switchValueDefault: SwitchValue = {
  version: 1,
  event: -1,
};

export const parseSwitchValue: ValueDeserializer = (packet: DeviceValuePacket): SwitchValue => {
  const { data } = packet;
  const version: number = data.readUInt8(0);

  return {
    version,
    event: data.readUInt8(1),
  };
};

export const createSwitchValue: ValueSerializer = (deviceValue: DeviceValue): DeviceValuePacket => {
  const value: SwitchValue = deviceValue as SwitchValue;
  const data: Buffer = Buffer.allocUnsafe(1);
  data.writeUInt8(value.event, 0);

  return {
    cmd: MessageCommand.DigitalValue,
    data,
  };
};
