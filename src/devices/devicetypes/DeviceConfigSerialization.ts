import { ConfigDeserializer, ConfigSerializer, DeviceConfig } from './DeviceConfig.js';
import { createPWMDimmerConfig, parsePWMDimmerConfig } from './PWMDimmer/PWMDimmerConfig.js';
import { createSwitchConfig, parseSwitchConfig } from './Switch/SwitchConfig.js';
import { toHex } from '../../log/utils/toHex.js';
import { DeviceType } from '../constants/DeviceType.js';

const serializers: Record<DeviceType, ConfigSerializer> = {
  [DeviceType.SwitchCtrl]: createSwitchConfig,
  [DeviceType.PWMDimmerCtrl]: createPWMDimmerConfig,
};

const deserializers: Record<DeviceType, ConfigDeserializer> = {
  [DeviceType.SwitchCtrl]: parseSwitchConfig,
  [DeviceType.PWMDimmerCtrl]: parsePWMDimmerConfig,
};

export function serializeDeviceConfig(deviceType: DeviceType, config: DeviceConfig): Buffer {
  const serializer: ConfigSerializer | undefined = serializers[deviceType];
  if (!serializer) {
    throw new Error(`Unable to serialize configuration for unknown device type ${toHex(deviceType)}`);
  }

  const data: Buffer = serializer(config);
  const msgBuffer: Buffer = Buffer.alloc(8 + data.length);
  msgBuffer.writeUInt8(data.length, 0);
  msgBuffer.writeUInt8(config.version, 1);
  data.copy(msgBuffer, 8);
  return msgBuffer;
}

export function deserializeDeviceConfig(deviceType: DeviceType, data: Buffer): DeviceConfig {
  const deserializer: ConfigDeserializer | undefined = deserializers[deviceType];
  if (!deserializer) {
    throw new Error(`Unable to deserialize configuration for unknown device type ${toHex(deviceType)}`);
  }

  const size: number = data.readUInt8(0);
  const version: number = data.readUInt8(1);
  if (data.length !== size + 8) {
    throw new Error(
      `Invalid configuration message received. Length mismatch: ${data.length} vs ${size + 8} (received vs expected)`,
    );
  }

  return deserializer(version, data.slice(8));
}
