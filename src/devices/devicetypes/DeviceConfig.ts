/**
 * Base type for device configuration to ensure versioning
 */
export interface DeviceConfig {
  version: number;
}

export type ConfigSerializer = (config: DeviceConfig) => Buffer;

export type ConfigDeserializer = (version: number, data: Buffer) => DeviceConfig;
