import { ValueDeserializer, ValueSerializer, DeviceValue, DeviceValuePacket } from './DeviceValue.js';
import { createPWMDimmerValue, parsePWMDimmerValue } from './PWMDimmer/PWMDimmerValue.js';
import { createSwitchValue, parseSwitchValue } from './Switch/SwitchValue.js';
import { toHex } from '../../log/utils/toHex.js';
import { DeviceType } from '../constants/DeviceType.js';

const serializers: Record<DeviceType, ValueSerializer> = {
  [DeviceType.SwitchCtrl]: createSwitchValue,
  [DeviceType.PWMDimmerCtrl]: createPWMDimmerValue,
};

const deserializers: Record<DeviceType, ValueDeserializer> = {
  [DeviceType.SwitchCtrl]: parseSwitchValue,
  [DeviceType.PWMDimmerCtrl]: parsePWMDimmerValue,
};

export function serializeDeviceValue(deviceType: DeviceType, value: DeviceValue): DeviceValuePacket {
  const serializer: ValueSerializer | undefined = serializers[deviceType];
  if (!serializer) {
    throw new Error(`Unable to serialize value for unknown device type ${toHex(deviceType)}`);
  }

  return serializer(value);
}

export function deserializeDeviceValue(deviceType: DeviceType, packet: DeviceValuePacket): DeviceValue {
  const deserializer: ValueDeserializer | undefined = deserializers[deviceType];
  if (!deserializer) {
    throw new Error(`Unable to deserialize value for unknown device type ${toHex(deviceType)}`);
  }

  return deserializer(packet);
}
