import { MessageCommand } from '../constants/MessageCommand.js';

/**
 * Base type for device values to ensure versioning
 */
export interface DeviceValue {
  version: number;
}

export interface DeviceValuePacket {
  cmd: MessageCommand;
  data: Buffer;
}

export type ValueSerializer = (value: DeviceValue) => DeviceValuePacket;

export type ValueDeserializer = (packet: DeviceValuePacket) => DeviceValue;

export type CommandDetector = (value: DeviceValue) => MessageCommand;
