import { DeviceType } from '../constants/DeviceType.js';
import { DeviceConfig } from '../devicetypes/DeviceConfig.js';

export interface Device {
  /**
   * Serial number of the device for identification. 32bit integer
   */
  serial: number;
  /**
   * Id assigned to device on the bus. 8 bit number 0x00 to 0x7f
   */
  natId: number;
  /**
   * Self-assigned nat of the device. Needed to send the reply when giving a server-assigned nat.
   */
  returnToNatId?: number;
  /**
   * Type of device
   */
  deviceType: DeviceType;
  /**
   * Firmware version of the device
   */
  firmwareVersion?: number;
  /**
   * Hardware version of the device
   */
  hardwareVersion?: number;
  /**
   * Last known device configuration
   */
  config?: DeviceConfig;
  /**
   * Last response  from the device
   */
  lastSeen: Date;
}
