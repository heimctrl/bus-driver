import { CanModule } from './can/can.module.js';
import { ConfigModule } from './config/config.module.js';
import { DevicesModule } from './devices/devices.module.js';
import { ServerModule } from './server/server.module.js';
import { Module } from '@nestjs/common';

@Module({
  imports: [ConfigModule, CanModule, DevicesModule, ServerModule],
})
export class AppModule {}
