import { AppModule } from './app.module.js';
import { NestFactory } from '@nestjs/core';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter());
  await app.listen(process.env.PORT || 3000);
}

bootstrap()
  .then(() => {
    console.log('Application started');
  })
  .catch((err: Error) => console.error(err));
