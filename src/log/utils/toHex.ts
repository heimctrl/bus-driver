/**
 * Helper to print a number as hexadecimal
 * @param num
 * @returns
 */
export function toHex(num: number | undefined | null): string {
  if (typeof num !== 'number') return '';
  return num.toString(16).toUpperCase();
}
