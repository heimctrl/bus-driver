import { LogService } from './log.service.js';
import { CONFIGS } from '../enums/config-enums.js';
import { Test, TestingModule } from '@nestjs/testing';

describe('LogService', () => {
  let service: LogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LogService,
        {
          provide: CONFIGS.log,
          useValue: new (jest.fn<unknown, unknown[]>(() => ({})))(),
        },
      ],
    }).compile();

    service = module.get<LogService>(LogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
