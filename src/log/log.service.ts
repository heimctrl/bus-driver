import { CONFIGS } from '../enums/config-enums.js';
import { Injectable, Inject } from '@nestjs/common';

export interface LogOptions {
  level: keyof typeof LevelMap;
  prefix?: string;
}

enum LevelMap {
  error = 1,
  warn = 2,
  debug = 3,
  trace = 4,
}

// tslint:disable: no-console
@Injectable()
export class LogService {
  private level: number;

  constructor(
    @Inject(CONFIGS.log)
    private options: LogOptions,
  ) {
    this.level = LevelMap[options.level];
    this.info(`Log level: ${options.level}`);
  }

  /**
   * Get a logger which prefixes all output
   * @param prefix
   */
  public getPrefixedLogger(prefix: string): LogService {
    return new LogService({
      ...this.options,
      prefix,
    });
  }

  public isDebugEnabled(): boolean {
    return this.level >= LevelMap.debug;
  }

  public isTraceEnabled(): boolean {
    return this.level >= LevelMap.trace;
  }

  public error = (...args: unknown[]): void => {
    if (this.level < LevelMap.error) {
      return;
    }
    console.error('[ERROR]', ...this.insertPrefix(args));
  };

  public warn = (...args: unknown[]): void => {
    if (this.level < LevelMap.warn) {
      return;
    }
    console.warn(...this.insertPrefix(args));
  };

  public debug = (...args: unknown[]): void => {
    if (!this.isDebugEnabled()) {
      return;
    }
    console.log(...this.insertPrefix(args));
  };

  public trace = (...args: unknown[]): void => {
    if (!this.isTraceEnabled()) {
      return;
    }
    console.log(...this.insertPrefix(args));
  };

  public info = (...args: unknown[]): void => {
    console.info(...this.insertPrefix(args));
  };

  public lazyError = (fn: () => unknown[]): void => {
    if (this.level < LevelMap.error) {
      return;
    }
    console.error('[ERROR]', ...this.insertPrefix(fn()));
  };

  public lazyWarn = (fn: () => unknown[]): void => {
    if (this.level < LevelMap.warn) {
      return;
    }
    console.warn(...this.insertPrefix(fn()));
  };

  public lazyDebug = (fn: () => unknown[]): void => {
    if (!this.isDebugEnabled()) {
      return;
    }
    console.log(...this.insertPrefix(fn()));
  };

  public lazyTrace = (fn: () => unknown[]): void => {
    if (!this.isTraceEnabled()) {
      return;
    }
    console.log(...this.insertPrefix(fn()));
  };

  public lazyInfo = (fn: () => unknown[]): void => {
    console.info(...this.insertPrefix(fn()));
  };

  private insertPrefix(args: unknown[]): unknown[] {
    const prefix: string | undefined = this.options.prefix;
    if (!prefix || !args || args.length === 0) {
      return args;
    }

    // make sure we keep support for the first arg to be a format string
    if (typeof args[0] === 'string') {
      return [`${prefix} ${args[0]}`, ...args.slice(1)];
    }

    return [prefix, ...args];
  }
}
