import { LogService } from './log.service.js';
import { ConfigModule } from '../config/config.module.js';
import { Module } from '@nestjs/common';

@Module({
  imports: [ConfigModule],
  providers: [LogService],
  exports: [LogService],
})
export class LogModule {}
