export enum CONFIGS {
  log = 'log',
  server = 'server',
  database = 'database',
}

export enum LOG_LEVEL {
  DEBUG = 'debug',
  ERROR = 'error',
  WARN = 'warn',
  INFO = 'info',
  TRACE = 'trace',
}
