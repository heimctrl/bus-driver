import { CAN_SERVICE } from '../can/can.const.js';
import { CanService } from '../can/can.types.js';
import { LogService } from '../log/log.service.js';
import { Controller, Get, Inject } from '@nestjs/common';

@Controller('bus')
export class BusController {
  private log: LogService;

  constructor(log: LogService, @Inject(CAN_SERVICE) private can: CanService) {
    this.log = log.getPrefixedLogger('[BUS_PROXY]');
  }

  @Get('messages')
  proxyGetMessages(): string[] {
    return [];
  }
}
