import { BusController } from './bus.controller.js';
import { HomeAssistantController } from './ha.controller.js';
import { CanModule } from '../can/can.module.js';
import { ConfigModule } from '../config/config.module.js';
import { HAModule } from '../ha/ha.module.js';
import { LogModule } from '../log/log.module.js';
import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';

@Module({
  controllers: [HomeAssistantController, BusController],
  imports: [HttpModule, LogModule, ConfigModule, HAModule, CanModule],
})
export class ServerModule {}
