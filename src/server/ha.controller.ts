import { ServerConfig } from '../config/config.codecs.js';
import { CONFIGS } from '../enums/config-enums.js';
import { HAAuthService } from '../ha/ha.auth.service.js';
import { LogService } from '../log/log.service.js';
import { HttpService } from '@nestjs/axios';
import { Controller, Get, Inject, Response } from '@nestjs/common';
import { FastifyReply } from 'fastify';

@Controller('ui')
export class HomeAssistantController {
  private log: LogService;

  constructor(
    log: LogService,
    @Inject(CONFIGS.server)
    private serverConfig: ServerConfig,
    private haAuthService: HAAuthService,
    private httpService: HttpService,
  ) {
    this.log = log.getPrefixedLogger('[HA_PROXY]');
  }

  @Get('health')
  async getHealth(@Response() res: FastifyReply): Promise<FastifyReply> {
    return res.status(200).send({
      status: 'OK',
      message: 'UI is up',
    });
  }

  @Get('panel')
  async getPanel(@Response() res: FastifyReply): Promise<FastifyReply> {
    const { url, defaultDashboard } = this.serverConfig.homeassistant;
    const { room } = this.serverConfig;

    const panelUrl = `${url}${defaultDashboard}`.replace('{ROOM}', room);

    // redirect to HA until cors issue in HA is fixed
    // return res.redirect(302, panelUrl);

    return res.header('content-type', 'text/html').send(`
<!DOCTYPE HTML>
<html>
  <head>
    <style rel="stylesheet">
      html { height: 100%; }
      body { padding: 0; margin: 0; width: 100%; height: 100%; }
      .actions {
        position: absolute;
        bottom: 0;
        right: 20px;
        height: 20px;
        padding: 5px;
        background-color: #333;
        text-align: right;
        z-index: 2;
      }
      .actions a {
        color: #aaf;
        font-weight: bold;
      }
      iframe {
        display: block;
        padding: 0;
        margin: 0;
        width: 100%;
        height: 100%;
        border: none;
      }
    </style>
    <script type="application/javascript">
      // check if fullscreen api is available and switch to fullscreen
      if (!document.fullscreenEnabled) {
        document.getElementById('fullscreenButton').style.display = 'none';
      } else if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen();
      }

      function toggleFullscreen() {
        if (!document.fullscreenElement) {
          document.documentElement.requestFullscreen();
        } else if (document.exitFullscreen) {
          document.exitFullscreen();
        }
      }
    </script>
  </head>
  <body>
    <iframe name="ha-main-window" src="${panelUrl}"></iframe>
    <div class="actions">
      <a href="javascript:location.reload()">refresh</a>
      <a id="fullscreenButton" href="javascript:toggleFullscreen()">fullscreen</a>
    </div>
  </body>
</html>
      `);
  }
}
