HeimCtrl: CAN bus driver
========================

Runs on the raspberry pi wall panel and listens for all connected devices.

Provides a REST api to communicate with the devices and manages discovery.

# Docker

Build command

    docker build --platform linux/arm64 -t backplane:80/heimctrl/bus-driver:latest .
    docker push backplane:80/heimctrl/bus-driver:latest

